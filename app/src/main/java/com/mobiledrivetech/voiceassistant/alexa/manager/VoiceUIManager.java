package com.mobiledrivetech.voiceassistant.alexa.manager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amazon.aace.alexa.AlexaClient;
import com.amazon.autovoicechrome.AutoVoiceChromeController;
import com.amazon.autovoicechrome.util.AutoVoiceChromeState;
import com.bumptech.glide.Glide;
import com.mobiledrivetech.voiceassistant.alexa.R;
import com.mobiledrivetech.voiceassistant.alexa.VoiceConstants;
import com.mobiledrivetech.voiceassistant.alexa.WifiSettings;
import com.mobiledrivetech.voiceassistant.alexa.bean.BaseBean;
import com.mobiledrivetech.voiceassistant.alexa.bean.EventReceivedBean;
import com.mobiledrivetech.voiceassistant.alexa.bean.NavigationListBean;
import com.mobiledrivetech.voiceassistant.alexa.bean.NewsBean;
import com.mobiledrivetech.voiceassistant.alexa.bean.WeatherBean;
import com.mobiledrivetech.voiceassistant.alexa.impl.AlexaClient.AlexaClientHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.Authorization.AuthorizationHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.SpeechRecognizer.SpeechRecognizerHandler;
import com.mobiledrivetech.voiceassistant.alexa.picklist.NavigationNearbyListAdapter;

import java.net.MalformedURLException;
import java.net.URL;

public class VoiceUIManager {
    private String TAG = VoiceUIManager.class.getSimpleName();

    private Context mContext;
    private static VoiceUIManager mInstance;
    private WindowManager mWindowManager;
    private WindowManager.LayoutParams wmParams;
    private ConstraintLayout mFloatLayout;
    private FrameLayout mAudioWaveLayout;
    private FrameLayout mChatLayout;
    private View mWaveViewLayout;
    private View mNetworkView;
    private LayoutInflater mLayoutInflater;
    private TranslateAnimation mShowAction;
    private TranslateAnimation mHiddenAction;
    private Handler mMainHandler = new Handler(Looper.getMainLooper());
    /* AutoVoiceChrome Controller */
    private AutoVoiceChromeController mAutoVoiceChromeController;
    private AlexaClientHandler mAlexaClient;
    private SpeechRecognizerHandler mSpeechRecognizer;
    private AuthorizationHandler mCblAuthorization;

    private VoiceUIManager() {

    }

    public static VoiceUIManager getInstance() {
        if (mInstance == null) {
            synchronized (VoiceUIManager.class) {
                if (mInstance == null) {
                    mInstance = new VoiceUIManager();
                }
            }
        }
        return mInstance;
    }

    public void init(Context context, AutoVoiceChromeController autoVoiceChromeController, AlexaClientHandler alexaClient, SpeechRecognizerHandler speechRecognizer) {
        mContext = context;
        mAutoVoiceChromeController = autoVoiceChromeController;
        mAlexaClient = alexaClient;
        mSpeechRecognizer = speechRecognizer;
        mCblAuthorization = null;
        mLayoutInflater = LayoutInflater.from(mContext);
        createFloatView();
    }

    public void init(Context context, AutoVoiceChromeController autoVoiceChromeController, AlexaClientHandler alexaClient, SpeechRecognizerHandler speechRecognizer, AuthorizationHandler cblAuthorization) {
        mContext = context;
        mAutoVoiceChromeController = autoVoiceChromeController;
        mAlexaClient = alexaClient;
        mSpeechRecognizer = speechRecognizer;
        mCblAuthorization = cblAuthorization;
        mLayoutInflater = LayoutInflater.from(mContext);
        createFloatView();
    }

    public void release() {
        // AutoVoiceChrome cleanup
        if (mAutoVoiceChromeController != null) {
            mAutoVoiceChromeController.onDestroy();
        }
    }

    // Auto Voice Chrome initialize function

    /**
     * Set up {@link AutoVoiceChromeController}, registering it with various attention state
     * notifying components, and creating an {@link AutoVoiceChromePresenter} to present the events.
     */
    private void initAutoVoiceChrome() {
        // Initialize AutoVoiceChromeController by providing layout container on which AutoVoiceChrome view will be
        // added.
        mAutoVoiceChromeController.initialize((ViewGroup) mAudioWaveLayout);

        // Notify the controller about state changes like dialog state, connection status,
        // wakeword enabled/disabled etc.
        mAlexaClient.setAutoVoiceChromeController(mAutoVoiceChromeController);
        mSpeechRecognizer.setAutoVoiceChromeController(mAutoVoiceChromeController);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void createFloatView() {
        mMainHandler.post(() -> {
            wmParams = new WindowManager.LayoutParams();
            mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                wmParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
            } else {
                wmParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_DIALOG;
            }
            wmParams.format = PixelFormat.RGBA_8888;
            wmParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE /*| WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE*/;
            wmParams.gravity = Gravity.START | Gravity.TOP;
            wmParams.x = 0;
            wmParams.y = 0;
            wmParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            wmParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

            mFloatLayout = (ConstraintLayout) mLayoutInflater.inflate(R.layout.floating_window, null);
//            mPickListView = mFloatLayout.findViewById(R.id.lv_pick_list);
            mAudioWaveLayout = mFloatLayout.findViewById(R.id.voice_chrome_view_container);
            initAutoVoiceChrome();
            mWindowManager.addView(mFloatLayout, wmParams);

            mChatLayout = mFloatLayout.findViewById(R.id.chat_layout);
            mWaveViewLayout = mFloatLayout.findViewById(R.id.wave_view_layout);

            mNetworkView = mFloatLayout.findViewById(R.id.no_net_work);
            mNetworkView.findViewById(R.id.network_set).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideFloatWindow();
                    WifiSettings.openSetting(mContext);
                }
            });
        });
    }

    private void resetFloatWindow() {
        try {
            wmParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            mWindowManager.updateViewLayout(mFloatLayout, wmParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showFloatWindow() {
        mMainHandler.post(() -> {
            mChatLayout.removeAllViews();
            resetFloatWindow();
            if (mWaveViewLayout != null) {
                if (mFloatLayout.getVisibility() == View.VISIBLE)
                    return;
                mFloatLayout.setVisibility(View.VISIBLE);

                // Notify dialog state change to AutoVoiceChrome
                if (mAutoVoiceChromeController != null) {
                    if ((mAlexaClient.getConnectionStatus() == AlexaClient.ConnectionStatus.CONNECTED)) {
                        mAutoVoiceChromeController.onStateChanged(mAlexaClient.convertToAutoVoiceChromeState(mAlexaClient.getDialogState()));
                    } else {
                        // If Alexa is disconnected or pending and dialog state comes
                        // then AutoVoiceChrome should show red bar
                        mAutoVoiceChromeController.onStateChanged(AutoVoiceChromeState.IN_ERROR);
                    }
                }

                if (mShowAction == null) {
                    mShowAction = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                            Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                            -1.0f, Animation.RELATIVE_TO_SELF, 0.0f);
                    mShowAction.setDuration(500);
                    mShowAction.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            Log.d(TAG, "showFloatWindow onAnimationStart");
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            Intent intent = new Intent();
                            intent.setAction("voice_assistant_status_changed");
                            intent.putExtra("activated", true);
                            mContext.sendBroadcast(intent);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                            Log.d(TAG, "showFloatWindow onAnimationRepeat");
                        }
                    });
                    mShowAction.setFillAfter(true);
                }
                mWaveViewLayout.startAnimation(mShowAction);
            }
        });
    }

    public void hideFloatWindow() {
        mMainHandler.post(() -> {
            if (mWaveViewLayout != null) {
                if (mFloatLayout.getVisibility() == View.GONE)
                    return;
                if (mHiddenAction == null) {
                    mHiddenAction = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                            Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                            0.0f, Animation.RELATIVE_TO_SELF, -1.0f);
                    mHiddenAction.setDuration(500);
                    mHiddenAction.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            Log.d(TAG, "hideFloatWindow onAnimationStart");
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            //  mWindowManager.removeView(mFloatLayout);
                            mFloatLayout.setVisibility(View.GONE);
                            mNetworkView.setVisibility(View.GONE);
                            resetFloatWindow();
                            Log.d(TAG, "hideFloatWindow onAnimationEnd");
                            Intent intent = new Intent();
                            intent.setAction("voice_assistant_status_changed");
                            intent.putExtra("activated", false);
                            mContext.sendBroadcast(intent);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                            Log.d(TAG, "hideFloatWindow onAnimationRepeat");
                        }
                    });
                    mHiddenAction.setFillAfter(true);
                }
                mWaveViewLayout.startAnimation(mHiddenAction);
            }
        });
    }

    public void updateUI(final BaseBean bean) {
        if (bean == null)
            return;
        Log.d(TAG, "updateUI viewType =" + bean.getType());
        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                View view = null;
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                switch (bean.getType()) {
                    case VoiceConstants.WidgetType.TYPE_WIDGET_CONTENT:
                        NewsBean newsBean = (NewsBean) bean;
//                        mAudioWaveLayout.toSpeak();
                        view = mLayoutInflater.inflate(R.layout.content_view, null);
                        TextView titleView = view.findViewById(R.id.news_title_text);
                        TextView textView = view.findViewById(R.id.content_text_view);
                        titleView.setText(newsBean.getTitle());
                        textView.setText(newsBean.getContent());
                        textView.setMovementMethod(ScrollingMovementMethod.getInstance());

                        final View measureView = view;
                        view.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                            @Override
                            public boolean onPreDraw() {
                                measureView.getViewTreeObserver().removeOnPreDrawListener(this);
                                int height = measureView.getHeight();
                                wmParams.height = height + mContext.getResources().getDimensionPixelOffset(R.dimen.size_120_dp);
                                return true;
                            }
                        });

                        view.getViewTreeObserver().addOnDrawListener(new ViewTreeObserver.OnDrawListener() {
                            @Override
                            public void onDraw() {
                                mWindowManager.updateViewLayout(mFloatLayout, wmParams);
                            }
                        });
                        break;
                    case VoiceConstants.WidgetType.TYPE_WIDGET_WEATHER:
                        WeatherBean weatherBean = (WeatherBean) bean;
//                        mAudioWaveLayout.toSpeak();
                        view = mLayoutInflater.inflate(R.layout.weather_view, null);
                        ImageView weathericon = view.findViewById(R.id.img_weather);
                        TextView txt_weather = view.findViewById(R.id.txt_weather);
                        TextView txt_area = view.findViewById(R.id.txt_area);
                        TextView txt_temperature = view.findViewById(R.id.txt_temperature);
                        txt_weather.setText(weatherBean.getWeather());
                        txt_area.setText(weatherBean.getCity());
                        String format = mContext.getResources().getString(R.string.weather_tem_format);
                        String result = String.format(format, weatherBean.getLowTemperature(), weatherBean.getHighTemperature());
                        txt_temperature.setText(result);
                        try {
                            URL myurl = new URL(weatherBean.getCurrentWeatherIcon());
                            Glide.with(mContext).load(myurl).into(weathericon);
                        } catch (MalformedURLException e) {
                            Log.d(TAG, "can't load the picture");
                            Glide.with(mContext).load(R.drawable.ic_weather).into(weathericon);
                        }
                        wmParams.height = mContext.getResources().getDimensionPixelOffset(R.dimen.size_100_dp);
                        layoutParams.height = mContext.getResources().getDimensionPixelOffset(R.dimen.size_61_dp);
                        break;
                    case VoiceConstants.WidgetType.TYPE_WIDGET_STOCK:
//                        mAudioWaveLayout.toSpeak();
                        view = mLayoutInflater.inflate(R.layout.stock_view, null);
                        break;
                    case VoiceConstants.WidgetType.TYPE_WIDGET_NAVIGATION_LIST:
                        NavigationListBean navigationListBean = (NavigationListBean) bean;
                        view = mLayoutInflater.inflate(R.layout.recycler_view, null);
                        NavigationNearbyListAdapter navigationNearbyListAdapter = new NavigationNearbyListAdapter(navigationListBean);
                        RecyclerView recyclerView = view.findViewById(R.id.recycler_layout);
                        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
                        recyclerView.setAdapter(navigationNearbyListAdapter);
                        recyclerView.setHasFixedSize(true);
                        wmParams.height = mContext.getResources().getDimensionPixelOffset(R.dimen.size_120_dp);
                        navigationNearbyListAdapter.notifyDataSetChanged();
                        break;
//                    case VoiceConstants.WidgetType.TYPE_WIDGET_HELP:
////                        mAudioWaveLayout.toSpeak();
//                        view = mLayoutInflater.inflate(R.layout.help_view, null);
//                        break;
                    case VoiceConstants.WidgetType.TYPE_CBL_CODE:
                        EventReceivedBean eventReceivedBean = (EventReceivedBean) bean;
                        view = mLayoutInflater.inflate(R.layout.cbl_code_view, null);
                        TextView txt_cbl_code = view.findViewById(R.id.cbl_code);
                        txt_cbl_code.setText(eventReceivedBean.getReceivedCode());
                        Button btn_cbl_cancel = view.findViewById(R.id.cbl_button);
                        btn_cbl_cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mCblAuthorization.cancelAuth("alexa:cbl");
                                hideFloatWindow();
                            }
                        });
                        break;
                    default:
                        hideFloatWindow();
                        return;
                }
                mWindowManager.updateViewLayout(mFloatLayout, wmParams);
                mChatLayout.addView(view, layoutParams);
            }
        });

    }
}
