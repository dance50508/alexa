package com.mobiledrivetech.voiceassistant.alexa.impl.LocalMediaSource;

import android.content.Context;

import com.mobiledrivetech.voiceassistant.alexa.impl.Logger.LoggerHandler;

public class DABLocalMediaSource extends LocalMediaSourceHandler {
    String m_state = "IDLE";
    public DABLocalMediaSource(Context context, LoggerHandler logger) {
        super(context, logger, Source.DAB);
    }

    @Override
    protected void setPlaybackState(String state) {
        m_state = state;
    }

    @Override
    protected SupportedPlaybackOperation[] getSupportedPlaybackOperations() {
        return new SupportedPlaybackOperation[] {SupportedPlaybackOperation.PLAY, SupportedPlaybackOperation.PAUSE,
                SupportedPlaybackOperation.STOP, SupportedPlaybackOperation.PREVIOUS, SupportedPlaybackOperation.NEXT};
    }

    @Override
    protected ContentSelector[] getSupportedContentSelectors() {
        return new ContentSelector[] {ContentSelector.CHANNEL};
    }

    @Override
    protected String getSourcePlaybackState() {
        return m_state;
    }
}
