package com.mobiledrivetech.voiceassistant.alexa.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


public class BluetoothMonitorReceiver extends BroadcastReceiver {
    private String TAG = BluetoothMonitorReceiver.class.getSimpleName();

    /**
     * 蓝牙开关的监听ACTION
     */
    public static final String ACTION_CONNECTION_STATE_CHANGED = "android.bluetooth.headsetclient.profile.action.CONNECTION_STATE_CHANGED";
    public static final String ACTION_DOWNLOAD_COMPLETE = "android.bluetooth.pbapclient.profile.action.DOWNLOAD_COMPLETE";

    private static final int DOWNDLOAD_ALL_NO_BACK = 0;
    private static final int DOWNLOAD_ALL_FRONT = 1;
    private static final int DOWNLOAD_ALL_BACK = 2;
    private static final int NO_DOWNLOAD_USE_OLD = 3;
    private static final int REDOWNLOAD_ALL_CALL_LOGS = 4;
    private static final int REDOWNLOAD_ALL_CONTACTS = 5;
    private static final int REDOWNLOAD_ALL_CONTACTS_WITH_NO_CALL_LOGS = 6;
    private static final int DOWNLOAD_CACHE_STARTED = 7;
    private static final int DOWNLOAD_DATABASE_READY = 8;

    private static IBluetoothMonitorListener mMonitorListener;

    /**
     * 设置蓝牙连接状态回调
     *
     * @param listener
     */
    public static void setBluMonitorListener(IBluetoothMonitorListener listener) {
        mMonitorListener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d(TAG, "onReceive: action = " + action + " ,mMonitorListener = " + mMonitorListener);
        switch (action) {
            case BluetoothAdapter.ACTION_STATE_CHANGED:
                int blueState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0);
                switch (blueState) {
                    case BluetoothAdapter.STATE_TURNING_ON:
                        Log.d(TAG, "onReceive: 蓝牙正在打开");
                        break;
                    case BluetoothAdapter.STATE_ON:
                        Log.d(TAG, "onReceive: 蓝牙已经打开");
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        Log.d(TAG, "onReceive: 蓝牙正在关闭");
                        break;
                    case BluetoothAdapter.STATE_OFF:
                        Log.d(TAG, "onReceive: 蓝牙已经关闭");
                        if (mMonitorListener != null) {
                            mMonitorListener.onBluDisconnect();
                        }
                        break;
                }
                break;
            case BluetoothDevice.ACTION_ACL_CONNECTED:
                Log.d(TAG, "onReceive: 蓝牙设备已连接");
                if (mMonitorListener != null) {
                    mMonitorListener.onBluConnect();
                }
                break;
            case BluetoothDevice.ACTION_ACL_DISCONNECTED:
                Log.d(TAG, "onReceive: 蓝牙设备已断开");
                if (mMonitorListener != null) {
                    mMonitorListener.onBluDisconnect();
                }
                break;
            case ACTION_CONNECTION_STATE_CHANGED:
                int newState = intent.getIntExtra(BluetoothProfile.EXTRA_STATE, BluetoothProfile.STATE_DISCONNECTED);
                int preState = intent.getIntExtra(BluetoothProfile.EXTRA_PREVIOUS_STATE, BluetoothProfile.STATE_DISCONNECTED);
                Log.w(TAG, "newState = " + newState + (newState == BluetoothProfile.STATE_CONNECTED ? " ,newState已连接蓝牙" : " ,newState未连接蓝牙"));
                Log.w(TAG, "preState = " + preState + (preState == BluetoothProfile.STATE_CONNECTED ? " ,preState已连接" : "preState未连接"));

                if ((newState == BluetoothProfile.STATE_CONNECTED) && (preState != BluetoothProfile.STATE_CONNECTED)) {
                    if (mMonitorListener != null) {
                        mMonitorListener.onBluConnect();
                    }
                } else {
                    if (mMonitorListener != null) {
                        mMonitorListener.onBluDisconnect();
                    }
                }
                break;
            case ACTION_DOWNLOAD_COMPLETE:
                switch (intent.getIntExtra("type", 0)){
                    case NO_DOWNLOAD_USE_OLD:
                    case DOWNLOAD_DATABASE_READY:
                    case REDOWNLOAD_ALL_CONTACTS:
                    case REDOWNLOAD_ALL_CONTACTS_WITH_NO_CALL_LOGS:
                        mMonitorListener.onContactRenew();
                        break;
                }
                break;
        }
    }
}
