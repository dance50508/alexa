package com.mobiledrivetech.voiceassistant.alexa.eventbus;

public class FinalOperation {
    String action;
    boolean toast;

    public FinalOperation(String action, boolean toast) {
        this.action = action;
        this.toast = toast;
    }

    public boolean isToast() {
        return toast;
    }

    public void setToast(boolean toast) {
        this.toast = toast;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
