package com.mobiledrivetech.voiceassistant.alexa.bean;

import java.util.List;

public class PhoneContactBean{
    private List<Contacts> contacts;

    public List<Contacts> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contacts> contacts) {
        this.contacts = contacts;
    }

    public static class Contacts {
        private String id;
        private Name name;
        private List<PhoneNumbers> phoneNumbers;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Name getName() {
            return name;
        }

        public void setName(Name name) {
            this.name = name;
        }

        public List<PhoneNumbers> getPhoneNumbers() {
            return phoneNumbers;
        }

        public void setPhoneNumbers(List<PhoneNumbers> phoneNumbers) {
            this.phoneNumbers = phoneNumbers;
        }

        public static class Name {
            private String firstName;
            private String lastName;
            private String nickName;
            private String phoneticFirstName;
            private String phoneticLastName;

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getNickName() {
                return nickName;
            }

            public void setNickName(String nickName) {
                this.nickName = nickName;
            }

            public String getPhoneticFirstName() {
                return phoneticFirstName;
            }

            public void setPhoneticFirstName(String phoneticFirstName) {
                this.phoneticFirstName = phoneticFirstName;
            }

            public String getPhoneticLastName() {
                return phoneticLastName;
            }

            public void setPhoneticLastName(String phoneticLastName) {
                this.phoneticLastName = phoneticLastName;
            }
        }

        public static class PhoneNumbers {
            private String label;
            private String number;

            public String getLabel() {
                return label;
            }

            public void setLabel(String label) {
                this.label = label;
            }

            public String getNumber() {
                return number;
            }

            public void setNumber(String number) {
                this.number = number;
            }
        }
    }
}
