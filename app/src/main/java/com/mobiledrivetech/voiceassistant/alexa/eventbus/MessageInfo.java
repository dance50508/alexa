/*
 * Copyright (c) 2021 Cerence, Inc. All rights reserved.
 */

package com.mobiledrivetech.voiceassistant.alexa.eventbus;

public class MessageInfo {
    private String type;
    private String json;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }
}