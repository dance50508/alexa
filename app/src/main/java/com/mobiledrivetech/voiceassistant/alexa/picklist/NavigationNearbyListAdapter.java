package com.mobiledrivetech.voiceassistant.alexa.picklist;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.mobiledrivetech.voiceassistant.alexa.bean.NavigationListBean;
import com.mobiledrivetech.voiceassistant.alexa.databinding.ListNavigateNearbyBinding;

public class NavigationNearbyListAdapter extends RecyclerView.Adapter<NavigationNearbyListAdapter.NavigationNearbyViewHolder> {
    NavigationListBean navigationListBean;

    public NavigationNearbyListAdapter(NavigationListBean navigationListBean) {
        this.navigationListBean = navigationListBean;
    }

    @NonNull
    @Override
    public NavigationNearbyListAdapter.NavigationNearbyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListNavigateNearbyBinding listNavigateNearbyBinding = ListNavigateNearbyBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new NavigationNearbyViewHolder(listNavigateNearbyBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull NavigationNearbyViewHolder holder, int position) {
        if (navigationListBean != null) {
            NavigationListBean.DestinationItem destinationItem = navigationListBean.getDestinationItemList().get(position);
            ((NavigationNearbyViewHolder) holder).tvOrder.setText(String.valueOf(position + 1));
            ((NavigationNearbyViewHolder) holder).tvTitle.setText(destinationItem.getTitle());
            ((NavigationNearbyViewHolder) holder).tvDesc.setText(destinationItem.getAddress());
            ((NavigationNearbyViewHolder) holder).tvKm.setText(destinationItem.getDistance());
        }
    }

    @Override
    public int getItemCount() {
        return navigationListBean.getDestinationItemList().size();
    }

    static class NavigationNearbyViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView tvOrder, tvTitle, tvDesc, tvKm;

        public NavigationNearbyViewHolder(@NonNull ListNavigateNearbyBinding listNavigateNearbyBinding) {
            super(listNavigateNearbyBinding.getRoot());
            tvOrder = listNavigateNearbyBinding.tvOrder;
            tvTitle = listNavigateNearbyBinding.tvTitle;
            tvDesc = listNavigateNearbyBinding.tvDesc;
            tvKm = listNavigateNearbyBinding.tvKm;
        }
    }
}
