package com.mobiledrivetech.voiceassistant.alexa.bean;

import java.io.Serializable;

/**
 * APP应用信息
 */
public class AppInfo implements Serializable {

    /**
     * app名称
     */
    private String name;

    /**
     * app包名
     */
    private String packageName;

    /**
     * app版本
     */
    private String version;

    /**
     * app安装路径 如: /system/vendor/operator/app/CIBN-VST/CIBN-VST.packageName 或者 /dataPath/app/com.tencent.qqmusic-1/base.packageName
     */
    private String path;

    /**
     * app数据保存路径 如: /dataPath/user/0/net.myvst.v2 或者 /dataPath/user/0/com.tencent.qqmusic
     */
    private String dataPath;

    /**
     * 是否系统应用
     */
    private Boolean system;

    public AppInfo() {

    }

    public AppInfo(String name, String packageName, String version, String path, String dataPath, Boolean isSystem) {
        this.name = name;
        this.packageName = packageName;
        this.version = version;
        this.path = path;
        this.dataPath = dataPath;
        this.system = isSystem;
    }

    /**
     * 获取应用名称
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * 获取应用包名
     *
     * @return
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * 获取应用版本
     *
     * @return
     */
    public String getVersion() {
        return version;
    }

    /**
     * 获取APK安装路径
     *
     * @return
     */
    public String getPath() {
        return path;
    }

    /**
     * 获取APK数据保存路径
     *
     * @return
     */
    public String getDataPath() {
        return dataPath;
    }

    /**
     * 是否系统应用
     *
     * @return
     */
    public Boolean isSystem() {
        return system;
    }

    @Override
    public String toString() {
        return getName() + "##" + getPackageName() + "##" + getVersion() + "##" + getPath() + "##" + getDataPath() + "##" + isSystem();
    }

}
