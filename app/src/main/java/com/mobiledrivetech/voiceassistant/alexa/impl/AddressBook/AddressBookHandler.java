package com.mobiledrivetech.voiceassistant.alexa.impl.AddressBook;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;

import com.amazon.aace.addressbook.AddressBook;
import com.amazon.aace.addressbook.IAddressBookEntriesFactory;
import com.mobiledrivetech.voiceassistant.alexa.R;
import com.mobiledrivetech.voiceassistant.alexa.impl.Logger.LoggerHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * The handler class which handles contact upload, cancel and delete.
 */
public class AddressBookHandler extends AddressBook {
    private static final String sTag = "AddressBookHandler";

    private static final String sContactsSourceId = "0001";
    private static final String sNavigationFavoritesSourceId = "0002";
    private static final String[] sSourceIds = {sContactsSourceId, sNavigationFavoritesSourceId};

    public static final String CONTACTS_FILE_NAME = "Contacts.json";
    public static final String NAVIGATION_FAVORITES_FILE_NAME = "NavigationFavorites.json";

    private View mContactsUploadView;
    private TextView mContactsUploadAccessStatus;

    private View mNavigationFavoritesUploadView;
    private TextView mNavigationFavoritesUploadAccessStatus;

    private final Activity mActivity;
    private final Context mContext;
    private final LoggerHandler mLogger;

    public SwitchCompat contactUploadSwitch = null;
    public SwitchCompat navigationFavoritesUploadSwitch = null;
    private final File mSampleDataDir;

    public AddressBookHandler(Context context, final LoggerHandler logger, final File sampleDataDir) {
        mActivity = null;
        mContext = context;
        mLogger = logger;
        mSampleDataDir = sampleDataDir;
    }

    public AddressBookHandler(final Activity activity, final LoggerHandler logger, final File sampleDataDir) {
        mActivity = activity;
        mContext = activity.getApplicationContext();
        mLogger = logger;
        mSampleDataDir = sampleDataDir;
    }

    public void onInitialize() {
        initContactsUI();
        initNavigationFavoritesUI();
        new LoadNavigationFavoritesTask().execute();
    }

    private void initContactsUI() {
        if (mActivity == null)  return;
        mContactsUploadView = mActivity.findViewById(R.id.contacts_uploader);
        mContactsUploadAccessStatus = mContactsUploadView.findViewById(R.id.access_to_contacts);

        View switchItem = mActivity.findViewById(R.id.toggle_contacts_upload);
        ((TextView) switchItem.findViewById(R.id.text)).setText(R.string.permit_upload);
        contactUploadSwitch = switchItem.findViewById(R.id.drawerSwitch);
        contactUploadSwitch.setChecked(false);

        // sets the listener on the phone connection toggle controller
        contactUploadSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    updateContacts();
//                    showPermissionDialogToAccessContacts();
                } else {
//                    mContactsUploadAccessStatus.setText(R.string.denied_access_to_contacts);
                    removeAddressBook(sContactsSourceId);
                }
            }
        });
    }

    public boolean updateContacts(){
        removeAddressBook(sContactsSourceId);
        return  addAddressBook(sContactsSourceId, "PhoneBook", AddressBookType.CONTACT);
    }

    public void removeContacts(){
        removeAddressBook(sContactsSourceId);
    }

    private void setToggleToUnchecked(int resNum) {
        if (mActivity == null)  return;
        View switchItem = mActivity.findViewById(resNum);
        SwitchCompat contactUploadSwitch = switchItem.findViewById(R.id.drawerSwitch);
        contactUploadSwitch.setChecked(false);
    }

    private void showPermissionDialogToAccessContacts() {
        if (mActivity == null)  return;
        final DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        mContactsUploadAccessStatus.setText(R.string.granted_access_to_contacts);

                        addAddressBook(sContactsSourceId, "PhoneBook", AddressBookType.CONTACT);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        mContactsUploadAccessStatus.setText(R.string.denied_access_to_contacts);

                        setToggleToUnchecked(R.id.toggle_contacts_upload);
                        break;
                }
            }
        };

        final DialogInterface.OnCancelListener cancelListener = new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                setToggleToUnchecked(R.id.toggle_contacts_upload);
            }
        };

        new AlertDialog.Builder(mActivity)
                .setMessage(R.string.seek_access_to_contacts)
                .setPositiveButton(R.string.confirm, clickListener)
                .setNegativeButton(R.string.deny, clickListener)
                .setOnCancelListener(cancelListener)
                .create()
                .show();
    }

    private void initNavigationFavoritesUI() {
        if (mActivity == null)  return;
        mNavigationFavoritesUploadView = mActivity.findViewById(R.id.navigation_favorites_uploader);
        mNavigationFavoritesUploadAccessStatus =
                mNavigationFavoritesUploadView.findViewById(R.id.access_to_navigation_favorites);
        // mContactsUploadAccessStatus.setVisibility(View.VISIBLE);

        View switchItem = mActivity.findViewById(R.id.toggle_navigation_favorites_upload);
        ((TextView) switchItem.findViewById(R.id.text)).setText(R.string.permit_upload);
        navigationFavoritesUploadSwitch = switchItem.findViewById(R.id.drawerSwitch);
        navigationFavoritesUploadSwitch.setChecked(false);

        // sets the listener on the phone connection toggle controller
        navigationFavoritesUploadSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    updateNavigationFavorites();
//                    showPermissionDialogToAccessNavigationFavorites();
                } else {
//                    mNavigationFavoritesUploadAccessStatus.setText(R.string.denied_access_to_navigation_favorites);
                    removeAddressBook(sNavigationFavoritesSourceId);
                }
            }
        });
    }

    public boolean updateNavigationFavorites(){
        removeAddressBook(sNavigationFavoritesSourceId);
        return  addAddressBook(sNavigationFavoritesSourceId, "NavigationFavorites", AddressBookType.NAVIGATION);
    }

    private void showPermissionDialogToAccessNavigationFavorites() {
        if (mActivity == null)  return;
        final DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        mNavigationFavoritesUploadAccessStatus.setText(R.string.granted_access_to_navigation_favorites);

                        addAddressBook(sNavigationFavoritesSourceId, "NavigationFavorites", AddressBookType.NAVIGATION);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        mNavigationFavoritesUploadAccessStatus.setText(R.string.denied_access_to_navigation_favorites);

                        setToggleToUnchecked(R.id.toggle_navigation_favorites_upload);
                        break;
                }
            }
        };

        final DialogInterface.OnCancelListener cancelListener = new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                setToggleToUnchecked(R.id.toggle_navigation_favorites_upload);
            }
        };

        new AlertDialog.Builder(mActivity)
                .setMessage(R.string.seek_access_to_navigation_favorites)
                .setPositiveButton(R.string.confirm, clickListener)
                .setNegativeButton(R.string.deny, clickListener)
                .setOnCancelListener(cancelListener)
                .create()
                .show();
    }

    private JSONObject parseFileAsJSONObject(String filePath) throws IOException, JSONException {
        mLogger.postInfo(sTag, String.format("parsing JSON from %s", filePath));
        File file = new File(filePath);
        FileInputStream is = new FileInputStream(file);
        byte[] buffer = new byte[is.available()];
        is.read(buffer);
        String json = new String(buffer, "UTF-8");
        JSONObject obj = new JSONObject(json);
        return obj;
    }

    private boolean parseFileAsContacts(String filename, IAddressBookEntriesFactory factory) {
        try {
            JSONObject json = parseFileAsJSONObject(filename);
            JSONArray contacts = json.getJSONArray("contacts");
            for (int i = 0; i < contacts.length(); ++i) {
                JSONObject contact = contacts.getJSONObject(i);
                JSONObject payload = new JSONObject();

                String id = contact.getString("id");
                if (id.isEmpty()) {
                    mLogger.postError(sTag, String.format("contactsIdEmpty"));
                    return false;
                }
                payload.put("entryId", id);
                payload.put("name", contact.getJSONObject("name"));

                if (contact.has("phoneNumbers")) {
                    payload.put("phoneNumbers", contact.getJSONArray("phoneNumbers"));
                }

                factory.addEntry(payload.toString());
            }
            return true;
        } catch (IOException e) {
            mLogger.postError(
                    sTag, String.format("Cannot read %s from assets directory. Error: %s", filename, e.getMessage()));
        } catch (JSONException e) {
            mLogger.postError(sTag, String.format("Cannot create json object. Error: %s", e.getMessage()));
        }

        return false;
    }

    private boolean parseFileAsNavigationFavorites(String filename, IAddressBookEntriesFactory factory) {
        try {
            JSONObject json = parseFileAsJSONObject(filename);
            JSONArray navigationFavorites = json.getJSONArray("navigationFavorites");
            for (int i = 0; i < navigationFavorites.length(); ++i) {
                JSONObject navigationFavorite = navigationFavorites.getJSONObject(i);
                JSONObject payload = new JSONObject();

                String id = navigationFavorite.getString("id");
                if (id.isEmpty()) {
                    mLogger.postError(sTag, String.format("navigationFavoriteIdEmpty"));
                    return false;
                }

                payload.put("entryId", id);
                payload.put("name", navigationFavorite.getJSONObject("name"));

                if (navigationFavorite.has("postalAddress")) {
                    JSONArray postalAddresses = new JSONArray();
                    postalAddresses.put(navigationFavorite.getJSONObject("postalAddress"));
                    payload.put("postalAddresses", postalAddresses);
                }
                factory.addEntry(payload.toString());
            }
            return true;
        } catch (IOException e) {
            mLogger.postError(
                    sTag, String.format("Cannot read %s from assets directory. Error: %s", filename, e.getMessage()));
        } catch (JSONException e) {
            mLogger.postError(sTag, String.format("Cannot create json object. Error: %s", e.getMessage()));
        }

        return false;
    }

    @Override
    public boolean getEntries(String contactsSourceId, IAddressBookEntriesFactory factory) {
        boolean success = false;

        if (contactsSourceId.equals(sContactsSourceId)) {
            // Always use sample data from external storage if available
//            File contactsFile = new File(Environment.getExternalStorageDirectory(), CONTACTS_FILE_NAME);
            File contactsFile = new File(mContext.getExternalFilesDir("data").getAbsolutePath(), CONTACTS_FILE_NAME);
            if (!contactsFile.exists()) {
                // Use the default from the assets folder
                contactsFile = new File(mSampleDataDir, CONTACTS_FILE_NAME);
            }

            success = parseFileAsContacts(contactsFile.getPath(), factory);
        } else if (contactsSourceId.equals(sNavigationFavoritesSourceId)) {
            // Always use sample data from external storage if available
//            File navigationFavoritesFile =
//                    new File(Environment.getExternalStorageDirectory(), NAVIGATION_FAVORITES_FILE_NAME);
            File navigationFavoritesFile =
                    new File(mContext.getExternalFilesDir("data").getAbsolutePath(), NAVIGATION_FAVORITES_FILE_NAME);
            if (!navigationFavoritesFile.exists()) {
                // Use the default from the assets folder
                navigationFavoritesFile = new File(mSampleDataDir, NAVIGATION_FAVORITES_FILE_NAME);
            }

            success = parseFileAsNavigationFavorites(navigationFavoritesFile.getPath(), factory);
        }

        mLogger.postInfo(sTag,
                String.format(
                        "success status of adding contacts/navigation with ID: (%s): (%b)", contactsSourceId, success));
        return success;
    }

    public void removeAllAddressBooks() {
        removeAddressBook("");
    }

    class LoadNavigationFavoritesTask extends AsyncTask<String, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... path) {
            File cacheDir = mContext.getCacheDir();
            File sampleDataDir = new File(cacheDir, "sampledata");
            File file = new File(sampleDataDir,"NavigationFavorites.json");
            try {
                FileReader fileReader = new FileReader(file);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                StringBuilder stringBuilder = new StringBuilder();
                String line = bufferedReader.readLine();
                while (line != null){
                    stringBuilder.append(line).append("\n");
                    line = bufferedReader.readLine();
                }
                bufferedReader.close();
                writeFile(stringBuilder.toString(), NAVIGATION_FAVORITES_FILE_NAME);
            } catch (IOException e) {
                Log.d(sTag+":IOException ", e.toString());
                e.printStackTrace();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            if (!bool) {
                return;
            }

            if (navigationFavoritesUploadSwitch != null){
                if (!navigationFavoritesUploadSwitch.isChecked()){
                    navigationFavoritesUploadSwitch.setChecked(true);
                    return;
                }
            }

            updateNavigationFavorites();
        }

        private void writeFile(String json, String fileName) {
            try {
//                File file = new File(Environment.getExternalStorageDirectory(), fileName);
                File file = new File(mContext.getExternalFilesDir("data").getAbsolutePath(), fileName);
                FileWriter fileWriter = new FileWriter(file);
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                bufferedWriter.write(json);
                bufferedWriter.close();
            } catch (IOException e) {
                Log.d(sTag+":IOException ", e.toString());
                e.printStackTrace();
            }
        }
    }
}
