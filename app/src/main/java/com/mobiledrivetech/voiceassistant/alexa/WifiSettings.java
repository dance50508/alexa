package com.mobiledrivetech.voiceassistant.alexa;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;

/**
 * WIFI设置工具类
 */
public class WifiSettings {

    /**
     * 打开WIFI设置界面
     *
     * @param context
     * @return
     */
    public static boolean openSetting(Context context) {
        //打开Wlan界面
        try {
            Intent intent = new Intent();
            intent.setClassName("com.android.settings", "com.android.settings.CarSettings");
            intent.setAction("set_wlan_action");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_WIFI_SETTINGS);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            try {
                context.startActivity(intent);
                return true;
            } catch (ActivityNotFoundException ex) {
                ex.printStackTrace();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return false;
    }


    /**
     * 打开WIFI
     */
    public static void openWifi(Context context) {
        android.net.wifi.WifiManager mWifiManager = (android.net.wifi.WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (!mWifiManager.isWifiEnabled()) {
            mWifiManager.setWifiEnabled(true);
        }
    }

    /**
     * 关闭WIFI
     */
    public static void closeWifi(Context context) {
        android.net.wifi.WifiManager mWifiManager = (android.net.wifi.WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (mWifiManager.isWifiEnabled()) {
            mWifiManager.setWifiEnabled(false);
        }
    }

}
