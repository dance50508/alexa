package com.mobiledrivetech.voiceassistant.alexa.bean;

import android.util.Log;
import android.util.Pair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NavigationListBean extends BaseBean {
    private final static String TAG = NavigationListBean.class.getSimpleName();
    private String name;    // 搜尋的單詞
    private List<DestinationItem> destinationItemList = new ArrayList<>();
    private int numOfItems;

    public int getNumOfItems() {
        return numOfItems;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DestinationItem> getDestinationItemList() {
        return destinationItemList;
    }

    public void setDestinationItemList(JSONArray pointOfInterests) throws JSONException {
        this.destinationItemList.clear();
        this.numOfItems = pointOfInterests.length();
        for (int i = 0; i < pointOfInterests.length(); i++) {
            JSONObject jsonObject = pointOfInterests.getJSONObject(i);
            DestinationItem destinationItem = new DestinationItem();
            destinationItem.setTitle(jsonObject.getJSONObject("title").getString("mainTitle"));
            destinationItem.setAddress(jsonObject.getString("address"));
            destinationItem.setCoordinate(Double.parseDouble(jsonObject.getJSONObject("coordinate").getString("longitudeInDegrees")), Double.parseDouble(jsonObject.getJSONObject("coordinate").getString("latitudeInDegrees")));
            destinationItem.setDistance(jsonObject.getString("travelDistance"));
            destinationItem.setTravelTime(jsonObject.getString("travelTime"));
            JSONObject iconUrl = new JSONObject(jsonObject.getJSONObject("image").getJSONArray("sources").getString(0));
            destinationItem.setDestinationIcon(iconUrl.getString("url"));
            Log.d(TAG, destinationItem.toString());
            this.destinationItemList.add(destinationItem);
        }
    }

    public class DestinationItem {
        private String title;
        private String address;
        private double longitudeInDegrees;
        private double latitudeInDegrees;
        private String distance;
        private String travelTime;
        private String destinationIcon;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Pair<Double, Double> getCoordinate() {
            return new Pair<>(longitudeInDegrees, latitudeInDegrees);
        }

        public void setCoordinate(Double longitude, Double latitude) {
            this.longitudeInDegrees = longitude;
            this.latitudeInDegrees = latitude;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getTravelTime() {
            return travelTime;
        }

        public void setTravelTime(String travelTime) {
            this.travelTime = travelTime;
        }

        public String getDestinationIcon() {
            return destinationIcon;
        }

        public void setDestinationIcon(String destinationIcon) {
            this.destinationIcon = destinationIcon;
        }

        public String toString() {
            return "DestinationItem{" +
                    "title='" + title + '\'' +
                    ", address='" + address + '\'' +
                    ", coordinate='" + longitudeInDegrees + ',' + latitudeInDegrees + '\'' +
                    ", distance='" + distance + '\'' +
                    ", travel time='" + travelTime + '\'' +
                    ", icon url='" + destinationIcon + '\'' +
                    '}' + "   ";
        }

    }
}
