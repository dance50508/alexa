package com.mobiledrivetech.voiceassistant.alexa;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.car.Car;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.graphics.Color;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.system.ErrnoException;
import android.system.Os;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;

import com.amazon.aace.alexa.AlexaProperties;
import com.amazon.aace.alexa.AlexaSpeaker;
import com.amazon.aace.alexa.SpeechRecognizer;
import com.amazon.aace.alexa.config.AlexaConfiguration;
import com.amazon.aace.audio.AudioOutputProvider;
import com.amazon.aace.core.Engine;
import com.amazon.aace.core.PlatformInterface;
import com.amazon.aace.core.config.ConfigurationFile;
import com.amazon.aace.core.config.EngineConfiguration;
import com.amazon.aace.navigation.config.NavigationConfiguration;
import com.amazon.aace.propertyManager.PropertyManager;
import com.amazon.aace.storage.config.StorageConfiguration;
import com.amazon.aace.vehicle.config.VehicleConfiguration;
import com.amazon.autovoicechrome.AutoVoiceChromeController;
import com.amazon.sampleapp.core.AuthStateObserver;
import com.amazon.sampleapp.core.AuthorizationHandlerFactoryInterface;
import com.amazon.sampleapp.core.AuthorizationHandlerObserverInterface;
import com.amazon.sampleapp.core.EngineStatusListener;
import com.amazon.sampleapp.core.LoggerControllerInterface;
import com.amazon.sampleapp.core.ModuleFactoryInterface;
import com.amazon.sampleapp.core.PropertyListener;
import com.amazon.sampleapp.core.SampleAppContext;
import com.google.android.gms.maps.model.LatLng;
import com.mobiledrivetech.voiceassistant.alexa.bluetooth.AlexaBluetoothManager;
import com.mobiledrivetech.voiceassistant.alexa.eventbus.FinalOperation;
import com.mobiledrivetech.voiceassistant.alexa.eventbus.MessageInfo;
import com.mobiledrivetech.voiceassistant.alexa.impl.AddressBook.AddressBookHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.Alerts.AlertsHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.AlexaClient.AlexaClientHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.AlexaSpeaker.AlexaSpeakerHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.Audio.AudioInputProviderHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.Audio.AudioOutputProviderHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.AudioPlayer.AudioPlayerHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.Authorization.AuthorizationHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.Authorization.CBLAuthorizationHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.CarControl.CarControlDataProvider;
import com.mobiledrivetech.voiceassistant.alexa.impl.CarControl.CarControlHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.DeviceSetup.DeviceSetupHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.DeviceUsage.DeviceUsageHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.DoNotDisturb.DoNotDisturbHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.EqualizerController.EqualizerConfiguration;
import com.mobiledrivetech.voiceassistant.alexa.impl.EqualizerController.EqualizerControllerHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.ExternalMediaPlayer.MACCPlayer;
import com.mobiledrivetech.voiceassistant.alexa.impl.LocalMediaSource.AMLocalMediaSource;
import com.mobiledrivetech.voiceassistant.alexa.impl.LocalMediaSource.BluetoothLocalMediaSource;
import com.mobiledrivetech.voiceassistant.alexa.impl.LocalMediaSource.CDLocalMediaSource;
import com.mobiledrivetech.voiceassistant.alexa.impl.LocalMediaSource.DABLocalMediaSource;
import com.mobiledrivetech.voiceassistant.alexa.impl.LocalMediaSource.DefaultMediaSource;
import com.mobiledrivetech.voiceassistant.alexa.impl.LocalMediaSource.FMLocalMediaSource;
import com.mobiledrivetech.voiceassistant.alexa.impl.LocalMediaSource.LineInLocalMediaSource;
import com.mobiledrivetech.voiceassistant.alexa.impl.LocalMediaSource.SatelliteLocalMediaSource;
import com.mobiledrivetech.voiceassistant.alexa.impl.LocalMediaSource.SiriusXMLocalMediaSource;
import com.mobiledrivetech.voiceassistant.alexa.impl.LocalMediaSource.USBLocalMediaSource;
import com.mobiledrivetech.voiceassistant.alexa.impl.LocationProvider.LocationProviderHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.Logger.LoggerHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.Messaging.MessagingHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.Navigation.NavigationHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.NetworkInfoProvider.NetworkInfoProviderHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.Notifications.NotificationsHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.PhoneCallController.PhoneCallControllerHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.PlaybackController.PlaybackControllerHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.PropertyManager.PropertyManagerHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.SpeechRecognizer.SpeechRecognizerHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.SpeechSynthesizer.SpeechSynthesizerHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.TemplateRuntime.TemplateRuntimeHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.TextToSpeech.TextToSpeechHandler;
import com.mobiledrivetech.voiceassistant.alexa.manager.VoiceUIManager;
import com.mobiledrivetech.voiceassistant.alexa.manager.VolumeChangeManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import skin.support.SkinCompatManager;
import skin.support.app.SkinAppCompatViewInflater;
import skin.support.app.SkinCardViewInflater;
import skin.support.constraint.app.SkinConstraintViewInflater;
import skin.support.design.app.SkinMaterialViewInflater;

public class VoiceAssistantService extends Service implements SampleAppContext {
    String TAG = this.getClass().getSimpleName();

    private static final int NOTIFICATION_VOICE_ASSISTANT_SERVICE = 1;

    private android.location.Location gpsLocation = new android.location.Location(LocationManager.GPS_PROVIDER),
            netwoekLocation = new android.location.Location(LocationManager.NETWORK_PROVIDER);
    private LocationManager mLocationManager;

    // AVS-supported locales: https://developer.amazon.com/en-US/docs/alexa/alexa-voice-service/system.html#locales
    private static final String[] sSupportedLocales = {"de-DE", "en-AU", "en-CA", "en-GB", "en-IN", "en-US", "es-ES",
            "es-MX", "es-US", "fr-CA", "fr-FR", "hi-IN", "it-IT", "ja-JP", "pt-BR", "en-CA/fr-CA", "en-IN/hi-IN",
            "en-US/es-US", "es-US/en-US", "fr-CA/en-CA", "hi-IN/en-IN"};

    /// Default list supported device locale
    private static final String[] sDefaultSupportedLocales = {"en-US", "en-GB", "de-DE", "en-IN", "en-CA", "ja-JP",
            "en-AU", "fr-FR", "it-IT", "es-ES", "es-MX", "fr-CA", "es-US", "hi-IN", "pt-BR"};

    /// Default list of supported dual locale
    private static final String[][] sDefaultSupportedDualLocales = {{"en-CA", "fr-CA"}, {"fr-CA", "en-CA"},
            {"en-US", "es-US"}, {"es-US", "en-US"}, {"en-IN", "hi-IN"}, {"hi-IN", "en-IN"}};


    /// Device default locale.
    private static final String sDefaultDeviceLocale = "en-US";

    /// Device default timezone
    private static final String sDefaultDeviceTimeZone = "America/Vancouver";

    /// Platform interface handlers
    private Map<String, PlatformInterface> mPlatformInterfaceHandlers = new HashMap<>();
    /* AACE Platform Interface Handlers */

    // Alexa
    private AlertsHandler mAlerts;
    private AlexaClientHandler mAlexaClient;
    private AudioPlayerHandler mAudioPlayer;
    private AuthorizationHandler mAuthorizationHandler;
    private EqualizerControllerHandler mEqualizerControllerHandler;
    private NotificationsHandler mNotifications;
    private PhoneCallControllerHandler mPhoneCallController;
    private AddressBookHandler mAddressBook;
    private PlaybackControllerHandler mPlaybackController;
    private SpeechRecognizerHandler mSpeechRecognizer;
    private SpeechSynthesizerHandler mSpeechSynthesizer;
    private TemplateRuntimeHandler mTemplateRuntime;
    private AlexaSpeakerHandler mAlexaSpeaker;
    private DoNotDisturbHandler mDoNotDisturb;
    private DeviceSetupHandler mDeviceSetupHandler;

    // Core
    private Engine mEngine;
    private boolean mEngineStarted = false;

    // Audio
    private AudioInputProviderHandler mAudioInputProvider;
    private AudioOutputProviderHandler mAudioOutputProvider;

    // Car Control
    private CarControlHandler mCarControlHandler;

    // Device Usage
    private DeviceUsageHandler mDeviceUsageHandler;

    // Location
    private LocationProviderHandler mLocationProvider;

    // Logger
    private LoggerHandler mLogger;

    // Messaging
    private MessagingHandler mMessaging;

    // Navigation
    private NavigationHandler mNavigation;

    // Network
    private NetworkInfoProviderHandler mNetworkInfoProvider;

    // Property Manager
    private PropertyManagerHandler mPropertyManager;

    // TextToSpeech
    private TextToSpeechHandler mTextToSpeech;

    /* Shared Preferences */
    private SharedPreferences mPreferences;

    private Map<String, String> mContextData = new HashMap<>();

    private MACCPlayer mMACCPlayer;

    private CDLocalMediaSource mCDLocalMediaSource;
    private DABLocalMediaSource mDABLocalMediaSource;
    private SiriusXMLocalMediaSource mSIRIUSXMLocalMediaSource;
    private AMLocalMediaSource mAMLocalMediaSource;
    private FMLocalMediaSource mFMLocalMediaSource;
    private BluetoothLocalMediaSource mBTLocalMediaSource;
    private LineInLocalMediaSource mLILocalMediaSource;
    private SatelliteLocalMediaSource mSATRADLocalMediaSource;
    private USBLocalMediaSource mUSBLocalMediaSource;
    private DefaultMediaSource mDefaultMediaSource;

    private LVCConfigReceiver mLVCConfigReceiver;

    /* AutoVoiceChrome Controller */
    private AutoVoiceChromeController mAutoVoiceChromeController;

//    // Locale
//    private Spinner mLocaleSpinnerView;
//    private ArrayAdapter<String> mLocaleAdapter;

    // A list of listeners to be notified of changes in the Engine lifecycle
    private List<EngineStatusListener> mEngineStatusListeners = new ArrayList<>();

    //Integrate theme observer
    private SettingsObserver mSettingsObserver;
    private ContentResolver mContentResolver;

    private Activity mActivity;
    private static Car mCar;

    public VoiceAssistantService() {
    }

    @Override
    public Activity getActivity() {
        return mActivity;
    }

    @Override
    public AudioOutputProvider getAudioOutputProvider() {
        return mAudioOutputProvider;
    }

    @Override
    public LoggerControllerInterface getLoggerController() {
        return mLogger;
    }

    @Override
    public SpeechRecognizer getSpeechRecognizer() {
        return mSpeechRecognizer;
    }

    @Override
    public PropertyManager getPropertyManager() {
        return (PropertyManager) mPropertyManager;
    }

    @Override
    public void registerPropertyListener(String name, PropertyListener propertyListener) {
        if (mPropertyManager != null) {
            mPropertyManager.registerListener(name, propertyListener);
        }
    }

    @Override
    public void addPlatformInterfaceHandler(String name, PlatformInterface handler) {
        mPlatformInterfaceHandlers.put(name, handler);
    }

    @Override
    public void addEngineStatusListener(EngineStatusListener listener) {
        synchronized (mEngineStatusListeners) {
            if (listener != null) {
                mEngineStatusListeners.add(listener);
            }
        }
    }

    @Override
    public void registerAuthStateObserver(AuthStateObserver observer) {
        if (mAlexaClient != null && observer != null) {
            mAlexaClient.registerAuthStateObserver(observer);
        }
    }

    @Override
    public ViewPager getViewPager() {
        return null;
    }

    @Override
    public String getData(String key) {
        return mContextData.get(key);
    }

    @Override
    public JSONObject getConfigFromFile(String configAssetName, String configRootKey) {
        JSONObject config = FileUtils.getOptionalConfigFromSDCard(configAssetName, configRootKey);
        if (config != null) {
            Log.i(TAG, "Got " + configRootKey + " from config file on the SD Card");
            return config;
        }
        return FileUtils.getConfigFromFile(getAssets(), configAssetName, configRootKey);
    }

    @Override
    public PlatformInterface getPlatformInterfaceHandler(String name) {
        if (mPlatformInterfaceHandlers.containsKey(name)) {
            return mPlatformInterfaceHandlers.get(name);
        }
        return null;
    }

    /**
     * Configure the Engine and register platform interface instances
     *
     * @param json JSON string with LVC config if LVC is supported, null otherwise.
     * @throws RuntimeException
     */
    private void startEngine(String json) throws RuntimeException {
        JSONObject debugConfig = getConfigFromFile("app_config.json", "debug");
        if (debugConfig != null && debugConfig.optBoolean("logSslKeys", false)) {
            try {
                File log = File.createTempFile("sslkey-aac", ".log");
                Os.setenv("SSLKEYLOGFILE", log.getAbsolutePath(), true);
                Log.i(TAG, "Log SSL keys to " + log.getAbsolutePath());
            } catch (ErrnoException | IOException e) {
                Log.e(TAG, "Failed to set SSLKEYLOGFILE: " + e.getMessage());
            }
        }

        // Create an "appdata" subdirectory in the cache directory for storing application data
        File cacheDir = getCacheDir();
        File appDataDir = new File(cacheDir, "appdata");
        File sampleDataDir = new File(cacheDir, "sampledata");

        // Copy certs from assets to certs subdirectory of cache directory
        File certsDir = new File(appDataDir, "certs");
        FileUtils.copyAllAssets(getAssets(), "certs", certsDir, false);

        // Copy models from assets to certs subdirectory of cache directory.
        // Force copy the models on every start so that the models on device cache are always the latest
        // from the APK
        File modelsDir = new File(appDataDir, "models");
        FileUtils.copyAllAssets(getAssets(), "models", modelsDir, true);

        copyAsset(AddressBookHandler.CONTACTS_FILE_NAME, new File(sampleDataDir, AddressBookHandler.CONTACTS_FILE_NAME),
                false);
        copyAsset(AddressBookHandler.NAVIGATION_FAVORITES_FILE_NAME,
                new File(sampleDataDir, AddressBookHandler.NAVIGATION_FAVORITES_FILE_NAME), false);
        copyAsset(MessagingHandler.CONVERSATIONS_REPORT_FILE_NAME,
                new File(sampleDataDir, MessagingHandler.CONVERSATIONS_REPORT_FILE_NAME), false);

        // Create AAC engine
        mEngine = Engine.create(this);

        ArrayList<EngineConfiguration> configuration = getEngineConfigurations(json, appDataDir, certsDir, modelsDir);

        // Get extra module factories and add configurations

        mContextData.put(SampleAppContext.CERTS_DIR, certsDir.getPath());
        mContextData.put(SampleAppContext.MODEL_DIR, modelsDir.getPath());
        mContextData.put(
                SampleAppContext.PRODUCT_DSN, mPreferences.getString(getString(R.string.preference_product_dsn), ""));
        mContextData.put(SampleAppContext.APPDATA_DIR, appDataDir.getPath());
        mContextData.put(SampleAppContext.JSON, json);

        List<ModuleFactoryInterface> extraFactories = getExtraModuleFactory();
        configExtraModules(this, extraFactories, configuration);

        EngineConfiguration[] configurationArray = configuration.toArray(new EngineConfiguration[configuration.size()]);
        boolean configureSucceeded = mEngine.configure(configurationArray);
        if (!configureSucceeded)
            throw new RuntimeException("Engine configuration failed");

        // Create the platform implementation handlers and register them with the engine
        // Logger
        if (!mEngine.registerPlatformInterface(mLogger = new LoggerHandler()))
            throw new RuntimeException("Could not register Logger platform interface");

        // AlexaClient
        if (!mEngine.registerPlatformInterface(mAlexaClient = new AlexaClientHandler(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register AlexaClient platform interface");

        // AudioInputProvider
        if (!mEngine.registerPlatformInterface(mAudioInputProvider = new AudioInputProviderHandler(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register AudioInputProvider platform interface");

        // EqualizerController
        if (!mEngine.registerPlatformInterface(
                mEqualizerControllerHandler = new EqualizerControllerHandler(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register EqualizerController platform interface");

        // AudioOutputProvider
        if (!mEngine.registerPlatformInterface(mAudioOutputProvider = new AudioOutputProviderHandler(
                getApplicationContext(), mLogger, mAlexaClient, mEqualizerControllerHandler)))
            throw new RuntimeException("Could not register AudioOutputProvider platform interface");

        // LocationProvider
        if (!mEngine.registerPlatformInterface(mLocationProvider = new LocationProviderHandler(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register LocationProvider platform interface");
        addEngineStatusListener(mLocationProvider);

        // Messaging
        if (!mEngine.registerPlatformInterface(mMessaging = new MessagingHandler(getApplicationContext(), mLogger, sampleDataDir)))
            throw new RuntimeException("Could not register MessagingController platform interface");

        // PhoneCallController
        if (!mEngine.registerPlatformInterface(mPhoneCallController = new PhoneCallControllerHandler(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register PhoneCallController platform interface");

        // PlaybackController
        if (!mEngine.registerPlatformInterface(mPlaybackController = new PlaybackControllerHandler(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register PlaybackController platform interface");

        // PropertyManager
        if (!mEngine.registerPlatformInterface(mPropertyManager = new PropertyManagerHandler(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register PropertyManager platform interface");

        // SpeechRecognizer
        // Note : Expects PropertyManager to be not null.
        if (!mEngine.registerPlatformInterface(
                mSpeechRecognizer = new SpeechRecognizerHandler(mAlexaClient, getApplicationContext(), mLogger, mPropertyManager)))
            throw new RuntimeException("Could not register SpeechRecognizer platform interface");

        // AudioPlayer
        if (!mEngine.registerPlatformInterface(mAudioPlayer = new AudioPlayerHandler(mLogger)))
            throw new RuntimeException("Could not register AudioPlayer platform interface");

        // SpeechSynthesizer
        if (!mEngine.registerPlatformInterface(mSpeechSynthesizer = new SpeechSynthesizerHandler()))
            throw new RuntimeException("Could not register SpeechSynthesizer platform interface");

        // TemplateRuntime
        if (!mEngine.registerPlatformInterface(
                mTemplateRuntime = new TemplateRuntimeHandler(mLogger, mPlaybackController)))
            throw new RuntimeException("Could not register TemplateRuntime platform interface");

        // AlexaSpeaker
        if (!mEngine.registerPlatformInterface(mAlexaSpeaker = new AlexaSpeakerHandler(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register AlexaSpeaker platform interface");

        // Alerts
        if (!mEngine.registerPlatformInterface(mAlerts = new AlertsHandler(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register Alerts platform interface");

        // NetworkInfoProvider
        if (!mEngine.registerPlatformInterface(
                mNetworkInfoProvider = new NetworkInfoProviderHandler(getApplicationContext(), mLogger, mPropertyManager)))
            throw new RuntimeException("Could not register NetworkInfoProvider platform interface");

        // Authorization
        if (!mEngine.registerPlatformInterface(mAuthorizationHandler = new AuthorizationHandler(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register Authorization platform interface");

        // Device Usage
        /**
         * Provide a way to disable network consumption reporting for test purposes. Production devices
         * must report network consumption.
         *
         * "deviceUsage" : {
         *      "recordNetworkUsage" : true
         *  }
         */
        JSONObject deviceUsageConfig = getConfigFromFile("app_config.json", "deviceUsage");
        boolean recordNetworkUsage = false;
        if (deviceUsageConfig != null) {
            recordNetworkUsage = deviceUsageConfig.optBoolean("recordNetworkUsage", false);
        }
        if (recordNetworkUsage) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!mEngine.registerPlatformInterface(mDeviceUsageHandler = new DeviceUsageHandler(this, getApplicationContext(), mLogger)))
                    throw new RuntimeException("Could not register DeviceUsage platform interface");
                addEngineStatusListener(mDeviceUsageHandler);
            } else {
                Log.w(TAG,
                        "Could not register DeviceUsage platform interface since Android API level is lower than Android 23 (M)");
                Toast.makeText(this, "Android API Level 23 required to register DeviceUsageHandler", Toast.LENGTH_LONG)
                        .show();
            }
        } else {
            Log.i(TAG, "Network usage consumption not enabled. Device Usage handler not registered");
        }

        JSONObject deviceConfig = new JSONObject();
        try {
            deviceConfig.put("productId", mPreferences.getString(getString(R.string.preference_product_id), ""));
            deviceConfig.put("productDsn", mPreferences.getString(getString(R.string.preference_product_dsn), ""));
        } catch (JSONException e) {
            throw new RuntimeException("Cloud not create a device json");
        }

        AuthorizationHandlerObserverInterface cblAuth = new CBLAuthorizationHandler(this, mLogger);
        cblAuth.initialize(mAuthorizationHandler, deviceConfig);

        List<AuthorizationHandlerFactoryInterface> extraAuthorizationModuleFactories =
                getExtraAuthorizationHandlerFactory();
        // loadAuthorizationHandlerUIAndRegister(extraAuthorizationModuleFactories, this);

        // Navigation
        if (!mEngine.registerPlatformInterface(mNavigation = new NavigationHandler(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register Navigation platform interface");

        // Notifications
        if (!mEngine.registerPlatformInterface(mNotifications = new NotificationsHandler(getApplicationContext(), mLogger))) {
            throw new RuntimeException("Could not register Notifications platform interface");
        }

        // DoNotDisturb
        if (!mEngine.registerPlatformInterface(mDoNotDisturb = new DoNotDisturbHandler(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register DoNotDisturb platform interface");
        else
            mAlexaClient.registerAuthStateObserver(mDoNotDisturb);

        // TextToSpeech
        if (!mEngine.registerPlatformInterface(mTextToSpeech = new TextToSpeechHandler(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register TextToSpeech platform interface");

        if (!mEngine.registerPlatformInterface(mAddressBook = new AddressBookHandler( this, mLogger, sampleDataDir))) {
            throw new RuntimeException("Could not register AddressBook platform interface");
        }

        if (!mEngine.registerPlatformInterface(mDeviceSetupHandler = new DeviceSetupHandler(this, mLogger)))
            throw new RuntimeException("Could not register Device Setup platform interface");

        // AlexaComms Handler

        // LVC Handlers
        if (!mEngine.registerPlatformInterface(mCarControlHandler = new CarControlHandler(this, mLogger))) {
            throw new RuntimeException("Could not register Car Control platform interface");
        }

        mMACCPlayer = new MACCPlayer(getApplicationContext(), mLogger);
        if (!mEngine.registerPlatformInterface(mMACCPlayer)) {
            Log.i("MACC", "registration failed");
            throw new RuntimeException("Could not register external media player platform interface");
        } else {
            Log.i("MACC", "registration succeeded");
        }
        mMACCPlayer.runDiscovery();

        // SiriusXM is disabled by default and enabled by setting mockSiriusXM to true in the
        // config file. For example:
        // {
        //   "lms": {
        //     "mockSiriusXM": true
        //   }
        // }
        boolean mockSiriusXM = false; // SiriusXM disabled by default
        JSONObject lmsConfig = CarAlexaAssistantApplication.getConfig(this, "lms");
        if (lmsConfig != null) {
            Log.i(TAG, "Using LMS config from file on SD card");
            mockSiriusXM = lmsConfig.optBoolean("mockSiriusXM", mockSiriusXM);
        }

        // Mock CD platform handler
        if (!mEngine.registerPlatformInterface(mCDLocalMediaSource = new CDLocalMediaSource(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register Mock CD player Local Media Source platform interface");

        // Mock DAB platform handler
        if (!mEngine.registerPlatformInterface(mDABLocalMediaSource = new DABLocalMediaSource(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register Mock DAB player Local Media Source platform interface");

        // Mock AM platform handler
        if (!mEngine.registerPlatformInterface(mAMLocalMediaSource = new AMLocalMediaSource(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register Mock AM radio player Local Media Source platform interface");

        // Mock SIRIUSXM platform handler
        if (mockSiriusXM
                && !mEngine.registerPlatformInterface(
                mSIRIUSXMLocalMediaSource = new SiriusXMLocalMediaSource(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register Mock SIRIUSXM player Local Media Source platform interface");

        // Mock FM platform handler
        if (!mEngine.registerPlatformInterface(mFMLocalMediaSource = new FMLocalMediaSource(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register Mock FM radio player Local Media Source platform interface");

        // Mock Bluetooth platform handler
        if (!mEngine.registerPlatformInterface(mBTLocalMediaSource = new BluetoothLocalMediaSource(getApplicationContext(), mLogger)))
            throw new RuntimeException(
                    "Could not register Mock Bluetooth player Local Media Source platform interface");

        // Mock Line In platform handler
        if (!mEngine.registerPlatformInterface(mLILocalMediaSource = new LineInLocalMediaSource(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register Mock Line In player Local Media Source platform interface");

        // Mock Satellite Radio platform handler
        if (!mEngine.registerPlatformInterface(mSATRADLocalMediaSource = new SatelliteLocalMediaSource(getApplicationContext(), mLogger)))
            throw new RuntimeException(
                    "Could not register Mock Satellite radio player Local Media Source platform interface");

        // Mock USB platform handler
        if (!mEngine.registerPlatformInterface(mUSBLocalMediaSource = new USBLocalMediaSource(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register Mock USB player Local Media Source platform interface");

        // Mock Default platform handler
        if (!mEngine.registerPlatformInterface(mDefaultMediaSource = new DefaultMediaSource(getApplicationContext(), mLogger)))
            throw new RuntimeException("Could not register Mock Default player Local Media Source platform interface");

//        // Register extra modules
//        loadPlatformInterfacesAndLoadUI(mEngine, extraFactories, this);
//
//        // Alexa Locale
//        mLocaleAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sSupportedLocales);
//
//        mLocaleSpinnerView = (Spinner) findViewById(R.id.locale_spinner);
//        mLocaleSpinnerView.setAdapter(mLocaleAdapter);
//
//        final String defaultLocale = mPropertyManager.getProperty(AlexaProperties.LOCALE);
//
//        int localePosition = mLocaleAdapter.getPosition(defaultLocale);
//        if (localePosition < 0) {
//            Log.e(TAG, defaultLocale + " is not in the Supported Locales");
//            localePosition = 0;
//        }
//        mLocaleSpinnerView.setSelection(localePosition);
//
//        mLocaleSpinnerView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
//                String s = sSupportedLocales[position];
//                if (!mPropertyManager.getProperty(AlexaProperties.LOCALE).equals(s)) {
//                    Toast.makeText(MainActivity.this, "Switching Alexa locale to " + s, Toast.LENGTH_SHORT).show();
//
//                    mPropertyManager.setProperty(AlexaProperties.LOCALE, s);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> arg0) {
//                // TODO Auto-generated method stub
//            }
//        });
//
//        // Timezone
//        setUpTimeZoneUI();

//        mPropertyManager.registerListener(AlexaProperties.TIMEZONE, this::updateTimezoneSpinner);
//        mPropertyManager.registerListener(AlexaProperties.LOCALE, this::updateLocaleSpinner);

        // Start the engine
        if (!mEngine.start())
            throw new RuntimeException("Could not start engine");
        mEngineStarted = true;

        synchronized (mEngineStatusListeners) {
            // Notify the EngineStatusListeners of the change in engine status
            for (EngineStatusListener listener : mEngineStatusListeners) {
                listener.onEngineStart();
            }
        }

//        // Check if Amazonlite is supported
//        if (mPropertyManager.getProperty(AlexaProperties.WAKEWORD_SUPPORTED).equals("true")) {
//            mSpeechRecognizer.enableWakeWordUI();
//        }
        mLogger.postInfo("Wakeword supported: ", mPropertyManager.getProperty(AlexaProperties.WAKEWORD_SUPPORTED));

        // log whether LocationProvider gave a supported country
        mLogger.postInfo("Country Supported: ", mPropertyManager.getProperty(AlexaProperties.COUNTRY_SUPPORTED));

        // Initialize AutoVoiceChrome
        initAutoVoiceChrome(mAuthorizationHandler);

        mAddressBook.onInitialize();
//        initTapToTalk();
//        initEarconsSettings();


        cblAuth.enable();
    }


    /**
     * Get the configurations to start the Engine
     *
     * @param json JSON string with LVC config if LVC is supported, null otherwise.
     * @param appDataDir path to app's data directory
     * @param certsDir path to certificates directory
     * @return List of Engine configurations
     */
    private ArrayList<EngineConfiguration> getEngineConfigurations(
            String json, File appDataDir, File certsDir, File modelsDir) {
        // Configure the engine
        String productDsn = mPreferences.getString(getString(R.string.preference_product_dsn), "");
        String clientId = mPreferences.getString(getString(R.string.preference_client_id), "");
        String productId = mPreferences.getString(getString(R.string.preference_product_id), "");

        AlexaConfiguration.TemplateRuntimeTimeout[] timeoutList = new AlexaConfiguration.TemplateRuntimeTimeout[] {
                new AlexaConfiguration.TemplateRuntimeTimeout(
                        AlexaConfiguration.TemplateRuntimeTimeoutType.DISPLAY_CARD_TTS_FINISHED_TIMEOUT, 8000),
                new AlexaConfiguration.TemplateRuntimeTimeout(
                        AlexaConfiguration.TemplateRuntimeTimeoutType.DISPLAY_CARD_AUDIO_PLAYBACK_FINISHED_TIMEOUT,
                        8000),
                new AlexaConfiguration.TemplateRuntimeTimeout(
                        AlexaConfiguration.TemplateRuntimeTimeoutType
                                .DISPLAY_CARD_AUDIO_PLAYBACK_STOPPED_PAUSED_TIMEOUT,
                        1800000)};

        String defaultLocale = sDefaultDeviceLocale;

        // Retrieve device setting configuration data from config file
        JSONObject deviceSettingsConfig = getConfigFromFile("app_config.json", "deviceSettings");
        if (deviceSettingsConfig != null) {
            // To override the default locale set deviceSettings.defaultLocale to desired value in
            // config file. For example:
            //  "deviceSettings": {
            //     "defaultLocale": "en-IN"
            //  }
            defaultLocale = deviceSettingsConfig.optString("defaultLocale", sDefaultDeviceLocale);
        }

        ArrayList<EngineConfiguration> configuration = new ArrayList<EngineConfiguration>(Arrays.asList(
                // AlexaConfiguration.createCurlConfig( certsDir.getPath(), "wlan0" ), Uncomment this line to specify
                // the interface name to use by AVS.
                AlexaConfiguration.createCurlConfig(certsDir.getPath()),
                AlexaConfiguration.createDeviceInfoConfig(
                        productDsn, clientId, productId, "Alexa Auto SDK", "Android Sample App"),
                AlexaConfiguration.createMiscStorageConfig(appDataDir.getPath() + "/miscStorage.sqlite"),
                AlexaConfiguration.createCertifiedSenderConfig(appDataDir.getPath() + "/certifiedSender.sqlite"),
                AlexaConfiguration.createCapabilitiesDelegateConfig(
                        appDataDir.getPath() + "/capabilitiesDelegate.sqlite"),
                AlexaConfiguration.createAlertsConfig(appDataDir.getPath() + "/alerts.sqlite"),
                AlexaConfiguration.createNotificationsConfig(appDataDir.getPath() + "/notifications.sqlite"),
                AlexaConfiguration.createDeviceSettingsConfig(appDataDir.getPath() + "/deviceSettings.sqlite",
                        sDefaultSupportedLocales, defaultLocale, sDefaultDeviceTimeZone, sDefaultSupportedDualLocales),
                AlexaConfiguration.createEqualizerControllerConfig(EqualizerConfiguration.getSupportedBands(),
                        EqualizerConfiguration.getMinBandLevel(), EqualizerConfiguration.getMaxBandLevel(),
                        EqualizerConfiguration.getDefaultBandLevels()),
                // Uncomment the below line to specify the speaker manager values
                // AlexaConfiguration.createSpeakerManagerConfig( true ),
                // Uncomment the below line to specify the template runtime values
                // AlexaConfiguration.createTemplateRuntimeTimeoutConfig( timeoutList ),
                StorageConfiguration.createLocalStorageConfig(appDataDir.getPath() + "/localStorage.sqlite"),

                // Create the optional navigation provider config name
                NavigationConfiguration.createNavigationConfig("HERE"),

                // Example Vehicle Config
                VehicleConfiguration.createVehicleInfoConfig(new VehicleConfiguration.VehicleProperty[] {
                        new VehicleConfiguration.VehicleProperty(
                                VehicleConfiguration.VehiclePropertyType.MAKE, "Amazon"),
                        new VehicleConfiguration.VehicleProperty(
                                VehicleConfiguration.VehiclePropertyType.MODEL, "AmazonCarOne"),
                        new VehicleConfiguration.VehicleProperty(
                                VehicleConfiguration.VehiclePropertyType.TRIM, "Advance"),
                        new VehicleConfiguration.VehicleProperty(VehicleConfiguration.VehiclePropertyType.YEAR, "2025"),
                        new VehicleConfiguration.VehicleProperty(
                                VehicleConfiguration.VehiclePropertyType.GEOGRAPHY, "US"),
                        new VehicleConfiguration.VehicleProperty(
                                VehicleConfiguration.VehiclePropertyType.VERSION, "1.0.0"),
                        new VehicleConfiguration.VehicleProperty(
                                VehicleConfiguration.VehiclePropertyType.OPERATING_SYSTEM,
                                "Android 8.1 Oreo API Level 26"),
                        new VehicleConfiguration.VehicleProperty(
                                VehicleConfiguration.VehiclePropertyType.HARDWARE_ARCH, "Armv8a"),
                        new VehicleConfiguration.VehicleProperty(
                                VehicleConfiguration.VehiclePropertyType.LANGUAGE, "en-US"),
                        new VehicleConfiguration.VehicleProperty(
                                VehicleConfiguration.VehiclePropertyType.MICROPHONE, "Single, roof mounted"),
                        // If this list is left blank, it will be fetched by the engine using amazon default endpoint
                        new VehicleConfiguration.VehicleProperty(
                                VehicleConfiguration.VehiclePropertyType.VEHICLE_IDENTIFIER, "123456789a")})));

        String endpointConfigPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/aace.json";
        if (new File(endpointConfigPath).exists()) {
            EngineConfiguration alexaEndpointsConfig = ConfigurationFile.create(
                    Environment.getExternalStorageDirectory().getAbsolutePath() + "/aace.json");
            configuration.add(alexaEndpointsConfig);
            Log.i("getEngineConfigurations", "Overriding endpoints");
        }

        // Provide a car control configuration to the Engine.
        //
        // This sample app facilitates feature testing by allowing the user to override the default
        // sample "aace.carControl" configuration generated by CarControlDataProvider by specifying
        // an alternative version in "CarControlConfig.json" on the SD card. The following
        // code checks for such an override to ensure the right config is provided to the Engine.
        // Note that your own application should not have this conditional SD card logic; The
        // car control configuration will be fixed and stable in a real integration.
        String sdCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        String externalCarControlConfigPath = sdCardPath + "/CarControlConfig.json";
        File externalCarControlConfigFile = new File(externalCarControlConfigPath);
        if (externalCarControlConfigFile.exists()) {
            Log.i(TAG, "Using car control config from file on SD card");
            EngineConfiguration carControlConfig =
                    ConfigurationFile.create(externalCarControlConfigFile.getAbsolutePath());
            configuration.add(carControlConfig);

            // CarControlDataProvider mocks representations of the controllable vehicle features for
            // testing purposes. The following ensures CarControlDataProvider sets up the right mock
            // features in the case where a configuration override is used.
            try {
                CarControlDataProvider.initialize(externalCarControlConfigFile.getAbsolutePath());
            } catch (Exception e) {
                Context context = getApplicationContext();
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, e.getMessage(), duration);
                toast.show();
            }
        } else {
            // Use programmatic generation of a default sample car control configuration.
            // The default config generated by CarControlDataProvider uses a custom asset defined
            // in "CarControlAssets.json" in this app's assets directory. We copy this file to the
            // cache directory so CarControlDataProvider can specify a path to this file in the
            // Engine configuration it generates.
            File sampleCustomAssetsFile = new File(appDataDir, "CarControlAssets.json");
            copyAsset("CarControlAssets.json", sampleCustomAssetsFile, true);
            EngineConfiguration ccConfig =
                    CarControlDataProvider.generateCarControlConfig(sampleCustomAssetsFile.getAbsolutePath());
            configuration.add(ccConfig);
        }

        return configuration;
    }

    /**
     * Start {@link LVCInteractionService}, the service that initializes and communicates with LVC,
     * and register a broadcast receiver to receive the configuration from LVC provided through the
     * {@link LVCInteractionService}
     */
    private void initLVC() {
        // Register broadcast receiver for configuration from the LVCInteractionService
        mLVCConfigReceiver = new LVCConfigReceiver();
        IntentFilter filter = new IntentFilter(LVCInteractionService.LVC_RECEIVER_INTENT);
        filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        LocalBroadcastManager.getInstance(this).registerReceiver(mLVCConfigReceiver, filter);

        // Start LVCInteractionService to communicate with LVC
        startService(new Intent(this, LVCInteractionService.class));
    }



    /**
     * Continue starting the Engine with the config received from LVC Service.
     *
     * @param config json string with LVC config if LVC is supported, null otherwise
     */
    private void onLVCConfigReceived(String config) {
        // Initialize AAC engine and register platform interfaces
        try {
            if (!mEngineStarted) {
                startEngine(config);
            } else {
                if (mActivity != null) {
//                    mHandler.post(new SetingUITask(mActivity));
                }
            }
        } catch (RuntimeException e) {
            Log.e(TAG, "Could not start engine. Reason: " + e.getMessage());
            return;
        }
        AlexaBluetoothManager.getInstance().init(getApplicationContext(), mPhoneCallController, mAddressBook);
//        VoiceUIManager.getInstance().hideFloatWindow();

        int volume = VolumeChangeManager.getInstance(getApplicationContext()).getCurrentMusicVolumeInAlexaUnit();
        Log.d(TAG, "init localSetVolume = "+volume);
        mAlexaSpeaker.localSetVolume(AlexaSpeaker.SpeakerType.ALEXA_VOLUME, (byte) volume);

        // Observe log event changes to update the log view
//        mLogger.addLogObserver(this);
//        mSpeechRecognizer.addObserver(this);
    }

    // Auto Voice Chrome initialize function
    /**
     * Set up {@link AutoVoiceChromeController}, registering it with various attention state
     * notifying components, and creating an {@link AutoVoiceChromePresenter} to present the events.
     */
    private void initAutoVoiceChrome(AuthorizationHandler cblAuth) {
        // Create AutoVoiceChromeController for your application to interact with AutoVoiceChrome.
        mAutoVoiceChromeController = new AutoVoiceChromeController(getApplicationContext());
        VoiceUIManager.getInstance().init(getApplicationContext(), mAutoVoiceChromeController,mAlexaClient,mSpeechRecognizer, cblAuth);
    }

    /**
     * Broadcast receiver to receive configuration from LVC provided through the
     * {@link LVCInteractionService}
     */
    class LVCConfigReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LVCInteractionService.LVC_RECEIVER_INTENT.equals(intent.getAction())) {
                if (intent.hasExtra(LVCInteractionService.LVC_RECEIVER_FAILURE_REASON)) {
                    // LVCInteractionService was unable to provide config from LVC
                    String reason = intent.getStringExtra(LVCInteractionService.LVC_RECEIVER_FAILURE_REASON);
                    onLVCConfigReceived(null);
                    Log.e(TAG, "Failed to init LVC: " + reason);
                } else if (intent.hasExtra(LVCInteractionService.LVC_RECEIVER_CONFIGURATION)) {
                    // LVCInteractionService received config from LVC
                    Log.i(TAG, "Received config from LVC, starting engine now");
                    String config = intent.getStringExtra(LVCInteractionService.LVC_RECEIVER_CONFIGURATION);
                    onLVCConfigReceived(config);
                }
            }
        }
    }

    /**
     * Retrieves a list of factory classes implementing @c AuthorizationHandlerFactoryInterface by
     * iterating through the json files that are located under the sample-app folder of assets
     * directory. The json contains the fully qualified class name of the factory class.
     * The expected json format:
     *
     * @code{.json}
     * {
     *
     *   "authorizationhandlerfactory": {
     *     "name": "fully-qualified-class-name-implementing-AuthorizationHandlerFactoryInterface"
     *   }
     *
     * }
     * @endcode
     */
    private List<AuthorizationHandlerFactoryInterface> getExtraAuthorizationHandlerFactory() {
        List<AuthorizationHandlerFactoryInterface> extraAuthorizationHandlerFactories = new ArrayList<>();
        try {
            String folderName = "sample-app";
            String factoryKey = "authorizationhandlerfactory";
            String category = "name";
            String[] fileList = getAssets().list(folderName);
            Log.i(TAG, "getExtraAuthorizationHandlerFactory: begin loading");
            for (String f : fileList) {
                InputStream is = getAssets().open(folderName + "/" + f);
                byte[] buffer = new byte[is.available()];
                is.read(buffer);
                String json = new String(buffer, "UTF-8");
                JSONObject obj = new JSONObject(json);
                if (obj != null) {
                    JSONObject factoryKeyObj = obj.optJSONObject(factoryKey);
                    if (factoryKeyObj == null) {
                        continue;
                    }
                    String factoryName = factoryKeyObj.getString(category);
                    AuthorizationHandlerFactoryInterface instance =
                            (AuthorizationHandlerFactoryInterface) Class.forName(factoryName).newInstance();
                    extraAuthorizationHandlerFactories.add(instance);
                    Log.i(TAG, "getExtraAuthorizationHandlerFactory: load extra module:" + factoryName);
                }
                is.close();
            }
        } catch (Exception e) {
            Log.e(TAG, "getExtraAuthorizationHandlerFactory: " + e.getMessage());
        }
        return extraAuthorizationHandlerFactories;
    }

    private void copyAsset(String assetPath, File destFile, boolean force) {
        FileUtils.copyAsset(getAssets(), assetPath, destFile, force);
    }

    private void updateDevicePreferences(String clientId, String productId, String productDsn) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(getString(R.string.preference_client_id), clientId);
        editor.putString(getString(R.string.preference_product_id), productId);
        editor.putString(getString(R.string.preference_product_dsn), productDsn);
        editor.apply();
    }

    /**
     * Retrieves a list of factory classes implementing @c ModuleFactoryInterface by
     * iterating through the json files that are located under the sample-app folder of assets
     * directory. The json contains the fully qualified class name of the factory class.
     * The expected json format:
     *
     * @code{.json}
     * {
     *
     *   "factory": {
     *     "name": "fully-qualified-class-name-implementing-ModuleFactoryInterface"
     *   }
     *
     * }
     * @endcode
     */
    private List<ModuleFactoryInterface> getExtraModuleFactory() {
        List<ModuleFactoryInterface> extraModuleFactories = new ArrayList<>();
        try {
            String folderName = "sample-app";
            String factoryKey = "factory";
            String category = "name";
            String[] fileList = getAssets().list(folderName);
            Log.i(TAG, "getExtraModuleFactory: begin loading");
            for (String f : fileList) {
                InputStream is = getAssets().open(folderName + "/" + f);
                byte[] buffer = new byte[is.available()];
                is.read(buffer);
                String json = new String(buffer, "UTF-8");
                JSONObject obj = new JSONObject(json);
                if (obj != null) {
                    JSONObject factoryKeyObj = obj.optJSONObject(factoryKey);
                    if (factoryKeyObj == null) {
                        continue;
                    }
                    String factoryName = factoryKeyObj.getString(category);
                    ModuleFactoryInterface instance = (ModuleFactoryInterface) Class.forName(factoryName).newInstance();
                    extraModuleFactories.add(instance);
                    Log.i(TAG, "load extra module: " + factoryName);
                }
                is.close();
            }
        } catch (Exception e) {
            Log.e(TAG, "getExtraModuleFactory: " + e.getMessage());
        }
        return extraModuleFactories;
    }


    //     Add configuration of extra modules
    private void configExtraModules(SampleAppContext sampleAppContext,
                                    List<ModuleFactoryInterface> extraModuleFactories, List<EngineConfiguration> configuration) {
        for (ModuleFactoryInterface moduleFactory : extraModuleFactories) {
            List<EngineConfiguration> moduleConfigs = moduleFactory.getConfiguration(sampleAppContext);
            for (EngineConfiguration moduleConfig : moduleConfigs) {
                Log.e(TAG, moduleConfig.toString());
                configuration.add(moduleConfig);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        runAsForeground();
        EventBus.getDefault().register(this);


        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        //Integrate theme observer
        mContentResolver = this.getContentResolver();
        mSettingsObserver = new SettingsObserver(new Handler());
        mContentResolver.registerContentObserver(Settings.Global.getUriFor(
                "current_theme"),
                false, mSettingsObserver);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: intent = " + intent);

        initVoiceAssistant();
        initSkinCompatManager();
        connectCar();

        // Initialize LVCInteractionService to start LVC, if supported
        initLVC();

        // Get shared preferences
        mPreferences = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        // Retrieve device config from config file and update preferences
        String clientId = "", productId = "", productDsn = "";

        JSONObject config = CarAlexaAssistantApplication.getConfig(this, "config");
        if (config != null) {
            try {
                clientId = config.getString("clientId");
                productId = config.getString("productId");
            } catch (JSONException e) {
                Log.w(TAG, "Missing device info in app_config.json");
            }
            try {
                productDsn = config.getString("productDsn");
            } catch (JSONException e) {
                try {
                    // set Android ID as product DSN
                    productDsn = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                    Log.i(TAG, "android id for DSN: " + productDsn);
                } catch (Error error) {
                    productDsn = UUID.randomUUID().toString();
                    Log.w(TAG, "android id not found, generating random DSN: " + productDsn);
                }
            }
        }
        updateDevicePreferences(clientId, productId, productDsn);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (mLogger != null)
            mLogger.postInfo(TAG, "Engine stopped");
        else
            Log.i(TAG, "Engine stopped");

        if (mLVCConfigReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mLVCConfigReceiver);
        }

        if (mNetworkInfoProvider != null) {
            mNetworkInfoProvider.unregister();
        }

        if (mMACCPlayer != null) {
            mMACCPlayer.cleanupMACCClient();
        }

        // cleanup for restarting sample app while using LVE
        if (mAddressBook != null) {
            mAddressBook.removeAllAddressBooks();
        }

        if (mEngine != null) {
            mEngine.dispose();
        }

        synchronized (mEngineStatusListeners) {
            for (EngineStatusListener listener : mEngineStatusListeners) {
                listener.onEngineStop();
            }
            mEngineStatusListeners.clear();
        }

        // AutoVoiceChrome cleanup
        if (mAutoVoiceChromeController != null) {
            mAutoVoiceChromeController.onDestroy();
        }

        VoiceUIManager.getInstance().release();
        super.onDestroy();
        destroyVoiceAssistant();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void runAsForeground() {
        String NOTIFICATION_CHANNEL_ID = "com.evenwell.clusterinfo";
        String channelName = "My Background Service";
        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setContentTitle("App is running in background")
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(NOTIFICATION_VOICE_ASSISTANT_SERVICE, notification);
    }

    private void connectCar() {

    }

    public static Car getConnectedCar(){
        return mCar;
    }

    protected void initVoiceAssistant() {
        Log.d(TAG, "Initializing Voice Assistant, Please wait...");

    }

    private void initSkinCompatManager(){
        Log.d(TAG, "initSkinCompatManager");
        SkinCompatManager.withoutActivity(getApplication())
                .addInflater(new SkinAppCompatViewInflater())           // 基础控件换肤初始化
                .addInflater(new SkinMaterialViewInflater())            // material design 控件换肤初始化[可选]
                .addInflater(new SkinConstraintViewInflater())          // ConstraintLayout 控件换肤初始化[可选]
                .addInflater(new SkinCardViewInflater())                // CardView v7 控件换肤初始化[可选]
                .setSkinStatusBarColorEnable(false)                     // 关闭状态栏换肤，默认打开[可选]
                .setSkinWindowBackgroundEnable(false)                   // 关闭windowBackground换肤，默认打开[可选]
                .loadSkin(CarAlexaAssistantApplication.ThemeType, new SkinCompatManager.SkinLoaderListener() {
                    @Override
                    public void onStart() {
                        Log.d(TAG, "initSkinCompatManager: onStart");
                    }

                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "initSkinCompatManager: onSuccess");

                    }

                    @Override
                    public void onFailed(String s) {
                        Log.d(TAG, "initSkinCompatManager: onFailed => "+s);
                    }
                }, SkinCompatManager.SKIN_LOADER_STRATEGY_INTERNAL_STORAGE);
    }

    protected void notifyContentChanged() {
        boolean hasGps = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean hasNetwork = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (hasGps) {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0F, new GpsLocationListener());
        }
        if (hasNetwork) {
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0F, new NetworkLocationListener());
        }

    }

    protected void destroyVoiceAssistant() {

    }

    protected void startVoiceAssistant() {

    }

    protected void stopVoiceAssistant() {
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(MessageInfo messageEvent) {
        Log.d(TAG, "onMessage: messageEvent = "+messageEvent.getJson());
//        finalAction.setText(messageEvent.getJson());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(String hints) {
        if (!TextUtils.isEmpty(hints)) {
            Log.d(TAG, "onMessage hints: "+hints);
//            finalAction.setText(hints);
        }
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onMessage(DialogEvent dialogEvent) {
//        MessageInfo info = new MessageInfo();
//        info.setType(dialogEvent.getName());
//        info.setJson(dialogEvent.getEvent().name());
//        Log.d(TAG, "onMessage: dialogEvent = "+dialogEvent.getEvent().name());
////        notificationAdapter.addData(info);
////        notificationRecycleView.scrollToPosition(notificationAdapter.getItemCount() - 1);
////        if (dialogEvent.getEvent() == DialogEvent.Event.CONVERSATION_END) {
////            PickListManager.get().hidePickList();
////        }
//    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onMessage(DialogStateChanged dialogStateChanged) {
//        MessageInfo info = new MessageInfo();
//        info.setType(dialogStateChanged.getName());
//        info.setJson(dialogStateChanged.getState().name());
//        Log.d(TAG, "onMessage: dialogStateChanged = "+dialogStateChanged.getState());
////        stateAdapter.addData(info);
////        stateRecycleView.scrollToPosition(stateAdapter.getItemCount() - 1);
//        if (dialogStateChanged.getState() == DialogStateChanged.State.IDLE ||
//                dialogStateChanged.getState() == DialogStateChanged.State.LISTENING_WUW) {
//            PickListManager.get().hidePickList();
//        }
//    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(FinalOperation operation) {
        if (operation.isToast()) {
            Toast.makeText(this, operation.getAction(), Toast.LENGTH_SHORT).show();
        }
        Log.d(TAG, "onMessage: operation = "+operation.getAction());

//        finalAction.setText(operation.getAction());
    }

    public class GpsLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(@NonNull android.location.Location location) {
            Date date = new Date(location.getTime());
            Log.d(TAG, "onGPSLocationChanged: " + date.toString() + "\tlatitude:" + location.getLatitude() + " longitude:" + location.getLongitude());
            gpsLocation = location;
            setLocationToVoiceAssistant();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(@NonNull String provider) {

        }

        @Override
        public void onProviderDisabled(@NonNull String provider) {

        }
    }

    public class NetworkLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(@NonNull android.location.Location location) {
            Date date = new Date(location.getTime());
            Log.d(TAG, "onNetworkLocationChanged: " + date.toString() + "\tlatitude:" + location.getLatitude() + " longitude:" + location.getLongitude());
            netwoekLocation = location;
            setLocationToVoiceAssistant();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(@NonNull String provider) {

        }

        @Override
        public void onProviderDisabled(@NonNull String provider) {
        }
    }


    private final class SettingsObserver extends ContentObserver {
        public SettingsObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            String type = Settings.Global.getString(mContentResolver, "current_theme");
            Log.d(TAG, "onChange: current_theme type = "+type);
            if(!type.equals(CarAlexaAssistantApplication.ThemeType)){
                CarAlexaAssistantApplication.ThemeType =type;
                setTheme();
            }
        }
    }
    private void setTheme(){
        Log.d(TAG, "setTheme: type ="+CarAlexaAssistantApplication.ThemeType);
        if(!CarAlexaAssistantApplication.ThemeType.equals(CarAlexaAssistantApplication.THEME_TYPE_DEF)) {
            SkinCompatManager.getInstance().loadSkin(CarAlexaAssistantApplication.ThemeType, null,
                    SkinCompatManager.SKIN_LOADER_STRATEGY_INTERNAL_STORAGE);
        } else {
            SkinCompatManager.getInstance().restoreDefaultTheme();

        }
    }

    private void setLocationToVoiceAssistant() {
        android.location.Location lastKnownLocationByGps = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (lastKnownLocationByGps != null) gpsLocation = lastKnownLocationByGps;
        android.location.Location lastKnownLocationByNetwork = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (lastKnownLocationByNetwork != null) netwoekLocation = lastKnownLocationByNetwork;

        android.location.Location currentLocation;
        double latitude = 0, longitude = 0;
        if (gpsLocation != null && netwoekLocation != null) {
            if (gpsLocation.getAccuracy() > netwoekLocation.getAccuracy()) {
                currentLocation = gpsLocation;
            } else {
                currentLocation = netwoekLocation;
            }
            latitude = currentLocation.getLatitude();
            longitude = currentLocation.getLongitude();
        }
        Log.d(TAG, "setLocationToCerence: latitude:" + latitude + " longitude:" + longitude);

        LatLng latLng = new LatLng(latitude, longitude);
    }

}