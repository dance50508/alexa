package com.mobiledrivetech.voiceassistant.alexa.bluetooth;

import android.annotation.SuppressLint;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.ContentResolver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.telecom.TelecomManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.internal.telephony.ITelephony;
import com.google.gson.Gson;
import com.mobiledrivetech.voiceassistant.alexa.MainActivity;
import com.mobiledrivetech.voiceassistant.alexa.R;
import com.mobiledrivetech.voiceassistant.alexa.bean.PhoneContactBean;
import com.mobiledrivetech.voiceassistant.alexa.impl.AddressBook.AddressBookHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.PhoneCallController.PhoneCallControllerHandler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class AlexaBluetoothManager {
    private static final String TAG = AlexaBluetoothManager.class.getSimpleName();
    private static final int HEADSET_CLIENT = 16;//BluetoothProfile.HEADSET_CLIENT
    private static final int PBAP_CLIENT = 17;//BluetoothProfile.PBAP_CLIENT=17

    private Context mContext;
    private static AlexaBluetoothManager mInstance;

    private BluetoothProfile mBluetoothHeadClient;
    private BluetoothProfile mBluetoothPBAPClient;

    private TelecomManager mTelecomManager;
    private PhoneCallControllerHandler mPhoneCallControllerHandler;
    private AddressBookHandler mAddressBookHandler;
    private int mCurrentState = TelephonyManager.CALL_STATE_IDLE;
    private int mOldState = TelephonyManager.CALL_STATE_IDLE;

    private SharedPreferences mPreferences;

    private AlexaBluetoothManager() {
    }

    public static AlexaBluetoothManager getInstance() {
        if (mInstance == null) {
            synchronized (AlexaBluetoothManager.class) {
                if (mInstance == null) {
                    mInstance = new AlexaBluetoothManager();
                }
            }
        }
        return mInstance;
    }

    public void init(Context context, PhoneCallControllerHandler phoneCallControllerHandler, AddressBookHandler addressBookHandler) {
        this.mContext = context;
        this.mTelecomManager = (TelecomManager) mContext.getSystemService(Context.TELECOM_SERVICE);
        this.mPhoneCallControllerHandler = phoneCallControllerHandler;
        this.mAddressBookHandler = addressBookHandler;
        this.mPreferences = mContext.getSharedPreferences(mContext.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        registPhoneState();
        registBluetoothReceiver();
        initBluetoothService();
    }

    /**
     * 监听蓝牙关闭和打开的状态
     */
    private void registBluetoothReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        intentFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        intentFilter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        intentFilter.addAction(BluetoothMonitorReceiver.ACTION_CONNECTION_STATE_CHANGED);
        intentFilter.addAction("android.bluetooth.pbapclient.profile.action.DOWNLOAD_COMPLETE");
        mContext.registerReceiver(new BluetoothMonitorReceiver(), intentFilter);
        BluetoothMonitorReceiver.setBluMonitorListener(mBluMonitorListener);
    }

    /**
     * 注册来去电状态监听
     */
    private void registPhoneState() {
        TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Service.TELEPHONY_SERVICE);
        telephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    private IBluetoothMonitorListener mBluMonitorListener = new IBluetoothMonitorListener() {
        @Override
        public void onBluConnect() {
            Log.d(TAG, "onBluConnect: 蓝牙已连接");
        }

        @Override
        public void onBluDisconnect() {
            if (mAddressBookHandler.contactUploadSwitch != null){
                mAddressBookHandler.contactUploadSwitch.setChecked(false);
            } else {
                mAddressBookHandler.removeContacts();
            }
        }

        @Override
        public void onContactRenew(){
            Log.d(TAG, "onContactRenew: 刷新通訊錄");
            new LoadContactsTask(mContext).execute();
        }
    };

    private void initBluetoothService() {
        Log.d(TAG, "initBluetoothService: ");
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothAdapter.getProfileProxy(mContext, mHeadServiceListener, HEADSET_CLIENT);
        bluetoothAdapter.getProfileProxy(mContext, mHfpServiceListener, PBAP_CLIENT);
    }

    /**
     * 判断蓝牙是否已经连接上
     *
     * @return
     */
    public boolean isBlueConn2() {
        boolean isConnected = false;
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Class<BluetoothAdapter> bluetoothAdapterClass = BluetoothAdapter.class;
        try {
            Method method = bluetoothAdapterClass.getDeclaredMethod("getConnectionState", (Class[]) null);
            //打开权限
            method.setAccessible(true);
            int state = (int) method.invoke(bluetoothAdapter, (Object[]) null);
            if (state == BluetoothAdapter.STATE_CONNECTED) {
                isConnected = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return isConnected;
        }
        Log.w(TAG, "isBlueConn: isConnected = " + isConnected);
        return isConnected;
    }

    /**
     * 判断蓝牙是否已经连接上
     *
     * @return
     */
    public boolean isBlueConn() {
        boolean isConnected = false;
        if (mBluetoothHeadClient != null) {
            List<BluetoothDevice> devices = mBluetoothHeadClient.getConnectedDevices();
            int size = devices.size();
            if (size > 0) {
                isConnected = true;
            }
        }
        Log.w(TAG, "isBlueConn: isConnected = " + isConnected);
        return isConnected;
    }


    private final BluetoothProfile.ServiceListener mHeadServiceListener = new BluetoothProfile.ServiceListener() {
        @Override
        public void onServiceConnected(int profile, BluetoothProfile proxy) {
            Log.d(TAG, "mHeadServiceListener onServiceConnected: profile = " + profile + ",proxy = " + proxy);
            if (profile == HEADSET_CLIENT) {
                mBluetoothHeadClient = proxy;
                if (mBluetoothHeadClient != null || mBluetoothPBAPClient != null) {
                    mPhoneCallControllerHandler.setPCCSwitch(true);
                    new LoadContactsTask(mContext).execute();
                }
            }
        }

        @Override
        public void onServiceDisconnected(int profile) {
            Log.d(TAG, "mHeadServiceListener onServiceDisconnected: " + profile);
            if (profile == HEADSET_CLIENT) {
                mBluetoothHeadClient = null;
                if (mBluetoothHeadClient == null && mBluetoothPBAPClient == null) {
                    mPhoneCallControllerHandler.setPCCSwitch(false);
                }
            }
        }
    };

    private final BluetoothProfile.ServiceListener mHfpServiceListener = new BluetoothProfile.ServiceListener() {
        @Override
        public void onServiceConnected(int profile, BluetoothProfile proxy) {
            Log.d(TAG, "mHfpServiceListener onServiceConnected: profile = " + profile + ",proxy = " + proxy);
            if (profile == PBAP_CLIENT) {
                mBluetoothPBAPClient = proxy;
                if (mBluetoothHeadClient != null || mBluetoothPBAPClient != null) {
                    mPhoneCallControllerHandler.setPCCSwitch(true);
                }
            }
        }

        @Override
        public void onServiceDisconnected(int profile) {
            Log.d(TAG, "mHfpServiceListener onServiceDisconnected: " + profile);
            if (profile == PBAP_CLIENT) {
                mBluetoothPBAPClient = null;
                if (mBluetoothHeadClient == null && mBluetoothPBAPClient == null) {
                    mPhoneCallControllerHandler.setPCCSwitch(false);
                }
            }
        }
    };

    private boolean isAuthSyncBlue() {
        if (mBluetoothPBAPClient != null) {
            List<BluetoothDevice> devices = mBluetoothPBAPClient.getConnectedDevices();
            int size = devices.size();
            if (size > 0) {
                Log.d(TAG, "isAuthBlue:: have blue permission ...");
                return true;
            }
            Log.d(TAG, "isAuthBlue:: not blue permission ...");
        }
        return false;
    }

    /**
     * 蓝牙是否连接
     *
     * @return
     */
    private boolean isHfpConnected() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Log.d(TAG, "mBluetoothAdapter is " + mBluetoothAdapter);
            return false;
        }
        int HEADSET_CLIENT = 16;
        boolean result = mBluetoothAdapter.getProfileConnectionState(HEADSET_CLIENT) == BluetoothProfile.STATE_CONNECTED;
        Log.d(TAG, "isHfpConnected = " + result);
        return result;
    }

    public boolean phoneCall(String number) {
        Log.d(TAG, "phoneCall: number=" + number);
        if (TextUtils.isEmpty(number)) {
            return false;
        }

        try {
            Uri uri = Uri.parse("tel:" + number);
            mTelecomManager.placeCall(uri, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void acceptCall() {
        Log.d(TAG, "acceptCall: ");
        try {
            mTelecomManager.acceptRingingCall();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 挂断
     */
    @SuppressLint({"NewApi", "MissingPermission"})
    public void endCall() {
        Log.d(TAG, "endCall: ");
        try {
            if (mTelecomManager == null) {
                mTelecomManager = (TelecomManager) mContext.getSystemService(Context.TELECOM_SERVICE);
            }
            mTelecomManager.endCall();
        } catch (Exception e) {
            try {
                e.printStackTrace();
                Method method = Class.forName("android.os.ServiceManager").getMethod("getService", String.class);
                IBinder binder = (IBinder) method.invoke(null, new Object[]{Context.TELEPHONY_SERVICE});
                ITelephony telephony = ITelephony.Stub.asInterface(binder);
                telephony.endCall();
                Log.i(TAG, "onRejectCall: 挂断电话 endCall ...");
            } catch (NoSuchMethodException e1) {
                e1.printStackTrace();
            } catch (ClassNotFoundException e2) {
                e2.printStackTrace();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    public void rejectCall() {
        Log.d(TAG, "rejectCall: ");
        try {
            endCall();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 通话状态监听器
     */
    public PhoneStateListener mPhoneStateListener = new PhoneStateListener() {

        @Override
        public void onCallStateChanged(int state, String phoneNumber) {
            super.onCallStateChanged(state, phoneNumber);

            mOldState = mPreferences.getInt(mContext.getString(R.string.preference_call_state), TelephonyManager.CALL_STATE_IDLE);

            switch (state) {
                case TelephonyManager.CALL_STATE_IDLE://無狀態
                    mCurrentState = TelephonyManager.CALL_STATE_IDLE;
                    mPhoneCallControllerHandler.onRemoteEnd();
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK://通話中
                    mCurrentState = TelephonyManager.CALL_STATE_OFFHOOK;
                    break;
                case TelephonyManager.CALL_STATE_RINGING://響鈴中
                    Log.i(TAG, "onCallStateChanged: ringing");
                    mCurrentState = TelephonyManager.CALL_STATE_RINGING;
                    mPreferences.edit().putInt(mContext.getString(R.string.preference_call_state), mCurrentState).apply();
                    mPhoneCallControllerHandler.onRemoteInitiate();
                    break;
            }

            if (mOldState == TelephonyManager.CALL_STATE_IDLE && mCurrentState == TelephonyManager.CALL_STATE_OFFHOOK) {
                Log.i(TAG, "onCallStateChanged: idle to offhook");
                mPreferences.edit().putInt(mContext.getString(R.string.preference_call_state), mCurrentState).apply();
                mPhoneCallControllerHandler.onRemoteAnswer();
            } else if (mOldState == TelephonyManager.CALL_STATE_RINGING && mCurrentState == TelephonyManager.CALL_STATE_OFFHOOK) {
                Log.i(TAG, "onCallStateChanged: ringing to offhook");
                mPreferences.edit().putInt(mContext.getString(R.string.preference_call_state), mCurrentState).apply();
                mPhoneCallControllerHandler.onRemoteAnswer();
            } else if (mOldState == TelephonyManager.CALL_STATE_OFFHOOK && mCurrentState == TelephonyManager.CALL_STATE_IDLE) {
                Log.i(TAG, "onCallStateChanged: offhook to endCall");
                mPreferences.edit().putInt(mContext.getString(R.string.preference_call_state), mCurrentState).apply();
                mPhoneCallControllerHandler.onRemoteEnd();
            } else if (mOldState == TelephonyManager.CALL_STATE_RINGING && mCurrentState == TelephonyManager.CALL_STATE_IDLE) {
                Log.i(TAG, "onCallStateChanged: ringing to rejecrCall");
                mPreferences.edit().putInt(mContext.getString(R.string.preference_call_state), mCurrentState).apply();
                mPhoneCallControllerHandler.onRemoteEnd();
            }
        }
    };

    class LoadContactsTask extends AsyncTask<String, Void, Boolean> {
        private Cursor cursor;

        public LoadContactsTask(Context context) {
            ContentResolver contentResolver = context.getContentResolver();
            cursor = contentResolver.query(
                    ContactsContract.Data.CONTENT_URI,
                    null, null, null, ContactsContract.Data.RAW_CONTACT_ID
            );
            Log.d(TAG, "cursor count: " + cursor.getCount() == null ? "0" : String.valueOf(cursor.getCount()));
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... path) {
            PhoneContactBean phoneContactBean = new PhoneContactBean();
            List<PhoneContactBean.Contacts> contactsList = new ArrayList<>();
            PhoneContactBean.Contacts contacts = null;
            PhoneContactBean.Contacts.Name name = null;
            List<PhoneContactBean.Contacts.PhoneNumbers> numList = null;
            int rawContactId = -1;
            cursor.moveToFirst();

            while (cursor.moveToNext()) {
                int currentId = cursor.getInt(cursor.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID));
                if (rawContactId != currentId) {
                    rawContactId = currentId;
                    if (name != null)
                        contacts.setName(name);
                    if (numList != null)
                        contacts.setPhoneNumbers(numList);
                    if (contacts != null)
                        contactsList.add(contacts);
                    contacts = new PhoneContactBean.Contacts();
                    contacts.setId(cursor.getString(cursor.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID)));
                    name = new PhoneContactBean.Contacts.Name();
                    numList = new ArrayList<>();
                }
                switch (cursor.getString(cursor.getColumnIndex(ContactsContract.Data.MIMETYPE))) {
                    case ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE:
                        name.setFirstName(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME)));
                        name.setLastName(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME)));
                        name.setPhoneticFirstName(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.PHONETIC_GIVEN_NAME)));
                        name.setPhoneticLastName(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.PHONETIC_FAMILY_NAME)));
                        break;
                    case ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE:
                        name.setNickName(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Nickname.NAME)));
                        break;
                    case ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE:
                        String label;
                        switch (cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE))) {
                            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                label = "Home";
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                label = "Mobile";
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                                label = "Work";
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_FAX_WORK:
                                label = "Fax Work";
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_FAX_HOME:
                                label = "Fax home";
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_PAGER:
                                label = "Pager";
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER:
                                label = "Other";
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_CALLBACK:
                                label = "Callback";
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_CAR:
                                label = "Car";
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_COMPANY_MAIN:
                                label = "Company main";
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_ISDN:
                                label = "ISDN";
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MAIN:
                                label = "Main";
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER_FAX:
                                label = "Other fax";
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_RADIO:
                                label = "Radio";
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_TELEX:
                                label = "Telex";
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_TTY_TDD:
                                label = "TTY/TDD";
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE:
                                label = "Work mobile";
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_PAGER:
                                label = "Work pager";
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_ASSISTANT:
                                label = "Assistant";
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MMS:
                                label = "MMS";
                                break;
                            default:
                                label = null;
                                break;
                        }
                        PhoneContactBean.Contacts.PhoneNumbers number = new PhoneContactBean.Contacts.PhoneNumbers();
                        number.setLabel(label);
                        number.setNumber(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                        numList.add(number);
                        break;
                }
            }
            if (contacts == null){
                Log.d(TAG, "contacts are null");
                return false;
            }
            contacts.setName(name);
            contacts.setPhoneNumbers(numList);
            contactsList.add(contacts);
            phoneContactBean.setContacts(contactsList);
            cursor.close();
            Gson gson = new Gson();
            writeFile(gson.toJson(phoneContactBean), AddressBookHandler.CONTACTS_FILE_NAME);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            if (!bool){
                return;
            }
            if (mAddressBookHandler.contactUploadSwitch != null){
                if (!mAddressBookHandler.contactUploadSwitch.isChecked()){
                    mAddressBookHandler.contactUploadSwitch.setChecked(true);
                    return;
                }
            }
            mAddressBookHandler.updateContacts();
        }

        private void writeFile(String json, String fileName) {
            try {
//                File file = new File(Environment.getExternalStorageDirectory(), fileName);
                File file = new File(mContext.getExternalFilesDir("data").getAbsolutePath(), fileName);
                FileWriter fileWriter = new FileWriter(file);
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                bufferedWriter.write(json);
                bufferedWriter.close();
            } catch (IOException e) {
                Log.d(TAG+":IOException ", e.toString());
                e.printStackTrace();
            }
        }
    }
}
