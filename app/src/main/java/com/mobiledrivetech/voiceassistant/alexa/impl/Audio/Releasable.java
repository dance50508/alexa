package com.mobiledrivetech.voiceassistant.alexa.impl.Audio;

/**
 * Used for releasing a resource.
 */
interface Releasable {
    void release();
}
