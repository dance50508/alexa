/*
 * Copyright 2017-2020 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *     http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.mobiledrivetech.voiceassistant.alexa.impl.SpeechRecognizer;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;
import com.amazon.aace.alexa.AlexaClient;
import com.amazon.aace.alexa.AlexaProperties;
import com.amazon.aace.alexa.SpeechRecognizer;
import com.mobiledrivetech.voiceassistant.alexa.R;
import com.mobiledrivetech.voiceassistant.alexa.impl.AlexaClient.AlexaClientHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.Logger.LoggerHandler;
import com.mobiledrivetech.voiceassistant.alexa.impl.PropertyManager.PropertyManagerHandler;

import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
// AutoVoiceChrome imports
import com.amazon.autovoicechrome.AutoVoiceChromeController;
import com.amazon.autovoicechrome.util.AutoVoiceChromeState;


public class SpeechRecognizerHandler extends SpeechRecognizer {
    private static final String TAG = SpeechRecognizerHandler.class.getSimpleName();

    private final Activity mActivity;
    private final Context mContext;
    private final LoggerHandler mLogger;
    private AudioCueObservable mAudioCueObservable = new AudioCueObservable();
    private final ExecutorService mExecutor = Executors.newFixedThreadPool(1);
    private boolean mAllowStopCapture = false; // Only true if holdToTalk() returned true
    private final View mToggleItem;
    private final View mMessage;
    private final TextView mLocaleMessage;
    private PropertyManagerHandler mPropertyManager;
    // AutoVoiceChrome controller
    private AutoVoiceChromeController mAutoVoiceChromeController;
    private final AlexaClientHandler mAlexaClient;


    public SpeechRecognizerHandler(AlexaClientHandler alexaClient, Context context, LoggerHandler logger, PropertyManagerHandler propertyManager) {
        mAlexaClient = alexaClient;
        mActivity = null;
        mContext = context;
        mLogger = logger;

        mToggleItem = null;
        mMessage = null;
        mLocaleMessage = null;

        mPropertyManager = propertyManager;
    }

    public SpeechRecognizerHandler(AlexaClientHandler alexaClient, Activity activity, LoggerHandler logger, PropertyManagerHandler propertyManager) {
        mAlexaClient = alexaClient;
        mActivity = activity;
        mContext = activity.getApplicationContext();
        mLogger = logger;

        // Toggle Wake Word switch
        mToggleItem = mActivity.findViewById(R.id.toggleWakeWord);
        ((TextView) mToggleItem.findViewById(R.id.text)).setText(R.string.wake_word_enabled);

        // Wake Word not supported message
        mMessage = mActivity.findViewById(R.id.wakeWordNotSupportedMessage);

        // Wakeword locale switching Message
        mLocaleMessage = mActivity.findViewById(R.id.wakeWordLocaleChangeMessage);

        mPropertyManager = propertyManager;

        disableWakeWordUI();
    }

    @Override
    public boolean wakewordDetected(String wakeWord) {
        mAudioCueObservable.playAudioCue(AudioCueState.START_VOICE);

        // Notify Error state to AutoVoiceChrome if disconnected with Alexa
        if (mAutoVoiceChromeController != null
                && mAlexaClient.getConnectionStatus() != AlexaClient.ConnectionStatus.CONNECTED) {
            mAutoVoiceChromeController.onStateChanged(AutoVoiceChromeState.IN_ERROR);
        }


        return true;
    }

    @Override
    public void endOfSpeechDetected() {
        mAudioCueObservable.playAudioCue(AudioCueState.END);
    }

    public void onTapToTalk() {
        if (tapToTalk())
            mAudioCueObservable.playAudioCue(AudioCueState.START_TOUCH);
    }

    public void onHoldToTalk() {
        mAllowStopCapture = false;
        if (holdToTalk()) {
            mAllowStopCapture = true;
            mAudioCueObservable.playAudioCue(AudioCueState.START_TOUCH);
        }
    }

    public void onReleaseHoldToTalk() {
        if (mAllowStopCapture)
            stopCapture();
        mAllowStopCapture = false;
    }

    private void disableWakeWordUI() {
        mToggleItem.setVisibility(View.GONE);
        mMessage.setVisibility(View.VISIBLE);
        mLocaleMessage.setVisibility(View.GONE);
    }

    public void enableWakeWordUI() {
        // Show toggle Wake Word option only if Wake Word supported
        final SwitchCompat wakeWordSwitch = mToggleItem.findViewById(R.id.drawerSwitch);
        mToggleItem.setVisibility(View.VISIBLE);
        mMessage.setVisibility(View.GONE);
        boolean wakeWordEnabled = mPropertyManager.getProperty(AlexaProperties.WAKEWORD_ENABLED).equals("true");
        wakeWordSwitch.setChecked(wakeWordEnabled);
        mLocaleMessage.setVisibility(View.VISIBLE);

        wakeWordSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mLogger.postInfo(TAG, "Enabling Wake Word");
                    mExecutor.submit(new Runnable() {
                        @Override
                        public void run() {
                            mPropertyManager.setProperty(AlexaProperties.WAKEWORD_ENABLED, "true");
                        }
                    });

                    mLocaleMessage.setVisibility(View.VISIBLE);
                } else {
                    mLogger.postInfo(TAG, "Disabling Wake Word");
                    mExecutor.submit(new Runnable() {
                        @Override
                        public void run() {
                            mPropertyManager.setProperty(AlexaProperties.WAKEWORD_ENABLED, "false");
                        }
                    });
                    mLocaleMessage.setVisibility(View.GONE);
                    }

                    // Notify wake word changes to AutoVoiceChrome
                    if (mAutoVoiceChromeController != null) {
                        mAutoVoiceChromeController.onStateChanged(
                                isChecked ? AutoVoiceChromeState.PRIVACY_MODE_OFF : AutoVoiceChromeState.PRIVACY_MODE_ON);
                    }
            }
        });
    }

    /* For playing speech recognition audio cues */

    public enum AudioCueState { START_TOUCH, START_VOICE, END }

    public static class AudioCueObservable extends Observable {
        void playAudioCue(AudioCueState state) {
            setChanged();
            notifyObservers(state);
        }
    }

    public void addObserver(Observer observer) {
        if (mAudioCueObservable == null)
            mAudioCueObservable = new AudioCueObservable();
        mAudioCueObservable.addObserver(observer);
    }

    // AutoVoiceChrome related functions
    /**
     * Set the {@link AutoVoiceChromeController} to receive notifications of the wakeword state
     *
     * @param controller  {@link AutoVoiceChromeController}
     */
    public void setAutoVoiceChromeController(AutoVoiceChromeController controller) {
        mAutoVoiceChromeController = controller;
    }
}
