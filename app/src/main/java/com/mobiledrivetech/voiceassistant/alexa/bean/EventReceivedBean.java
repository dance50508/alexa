package com.mobiledrivetech.voiceassistant.alexa.bean;

public class EventReceivedBean extends BaseBean {
    private String receivedCode;
    private String receivedUrl;
    private String receivedType;

    public String getReceivedCode() {
        return receivedCode;
    }

    public void setReceivedCode(String receivedCode) {
        this.receivedCode = receivedCode;
    }

    public String getReceivedUrl() {
        return receivedUrl;
    }

    public void setReceivedUrl(String receivedUrl) {
        this.receivedUrl = receivedUrl;
    }

    public String getReceivedType() {
        return receivedType;
    }

    public void setReceivedType(String receivedType) {
        this.receivedType = receivedType;
    }
}
