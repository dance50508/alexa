package com.mobiledrivetech.voiceassistant.alexa.bean;


public class BaseBean {
    private int type;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

}
