package com.mobiledrivetech.voiceassistant.alexa.bluetooth;

/**
 * 蓝牙状态接口
 */
public interface IBluetoothMonitorListener {

    /**
     * 蓝牙已连接
     */
    void onBluConnect();

    /**
     * 蓝牙已断开
     */
    void onBluDisconnect();

    /**
     * 通訊錄更新
     */
    void onContactRenew();
}
