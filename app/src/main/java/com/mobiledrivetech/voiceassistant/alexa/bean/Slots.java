package com.mobiledrivetech.voiceassistant.alexa.bean;

public class Slots {

    private String address;
    private String latitude;
    private String longitude;
    private String name;

    public Slots(String address,  String name,String latitude, String longitude) {
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
    }

    public void setAddress(String address) {
         this.address = address;
     }
     public String getAddress() {
         return address;
     }

    public void setLatitude(String latitude) {
         this.latitude = latitude;
     }
     public String getLatitude() {
         return latitude;
     }

    public void setLongitude(String longitude) {
         this.longitude = longitude;
     }
     public String getLongitude() {
         return longitude;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

}