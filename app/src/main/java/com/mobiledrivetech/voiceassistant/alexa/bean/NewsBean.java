package com.mobiledrivetech.voiceassistant.alexa.bean;

/**
 * @ClassName: NewsBean
 * @Description: java类作用描述
 * @CreateDate: 2020/5/27 14:40
 */
public class NewsBean extends BaseBean {
    private String title;//新闻标题
    private String content;//新闻内容

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
