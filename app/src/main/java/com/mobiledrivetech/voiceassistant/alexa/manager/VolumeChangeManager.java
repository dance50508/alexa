package com.mobiledrivetech.voiceassistant.alexa.manager;

import android.car.Car;
import android.car.media.CarAudioManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.util.Log;

import com.mobiledrivetech.voiceassistant.alexa.LVCInteractionService;

import java.lang.ref.WeakReference;

public class VolumeChangeManager {
    private static String TAG = "CarAlexa_" + VolumeChangeManager.class.getSimpleName();

    private static final String VOLUME_CHANGED_ACTION = "android.media.VOLUME_CHANGED_ACTION";
    private static final String EXTRA_VOLUME_STREAM_TYPE = "android.media.EXTRA_VOLUME_STREAM_TYPE";

    public interface VolumeChangeListener {
        /**
         * 系统媒体音量变化
         *
         * @param volume
         */
        void onVolumeChanged(int volume);
    }

    private VolumeChangeListener mVolumeChangeListener;
    private VolumeBroadcastReceiver mVolumeBroadcastReceiver;
    private Context mContext;
    private CarAudioManager mCarAudioManager;
    private AudioManager mAudioManager;

    private final int MAX_VOLUME;
    private boolean mRegistered = false;

    private static VolumeChangeManager mInstance;


    public VolumeChangeManager(Context context) {
        mContext = context;
        registerVolumeReceiver();

        if(LVCInteractionService.checkIsCarUiMode(mContext)) {
            mCarAudioManager = (CarAudioManager) LVCInteractionService.getConnectedCar().getCarManager(Car.AUDIO_SERVICE);
            MAX_VOLUME = mCarAudioManager.getGroupMaxVolume(0);
        } else {
            mAudioManager = (AudioManager)mContext.getSystemService(Context.AUDIO_SERVICE);
            MAX_VOLUME = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        }
    }

    public static VolumeChangeManager getInstance(Context context) {
        if (mInstance == null) {
            synchronized (VolumeChangeManager.class) {
                if (mInstance == null) {
                    mInstance = new VolumeChangeManager(context);
                }
            }
        }
        return mInstance;
    }



    /**
     * 以0-100为范围，获取当前媒体的音量值
     *
     * @return 当前音量
     */
    public int getCurrentMusicVolumeInAlexaUnit() {
        int currentVolume =0;
        int volume =0;

        if(mAudioManager!=null){
            currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            volume = 100 * currentVolume / MAX_VOLUME;
        } else if(mCarAudioManager!=null){
            currentVolume = mCarAudioManager.getGroupVolume(0);
            volume = 100 * currentVolume / MAX_VOLUME;
        }

        Log.d(TAG, "getCurrentVolume currentVolume= " + currentVolume + "volume=" + volume);
        return volume;
    }

    /**
     * volume 以0-100为范围，需要映射成系统的音量表示
     *
     * @return 设置之后的媒体音量值
     */
    public void setVolume(int volume) {
        int a = (int) Math.ceil((volume) * MAX_VOLUME * 0.01);
        a = a <= 0 ? 0 : a;
        a = a >= MAX_VOLUME ? MAX_VOLUME : a;
        Log.d(TAG, "setVolume a= " + a);
        
        if(mAudioManager!=null){
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, a,
                    AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
        } else if(mCarAudioManager!=null){
            mCarAudioManager.setGroupVolume( CarAudioManager.PRIMARY_AUDIO_ZONE, 0, a,
                    AudioManager.FLAG_SHOW_UI | AudioManager.FLAG_PLAY_SOUND);
        }


    }

    /**
     * volume 以0-1为范围，需要映射成系统的音量表示
     *
     * @return 设置之后的媒体音量值
     */
    public void setVolumeInPercentage(float volumePercentage) {
        int volumeInAudioManagerUnit;
        if(mAudioManager!=null){
            volumeInAudioManagerUnit = (int) (MAX_VOLUME * volumePercentage);
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                    volumeInAudioManagerUnit,
                    AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
        } else if(mCarAudioManager!=null){
            volumeInAudioManagerUnit = (int) (MAX_VOLUME * volumePercentage);
            mCarAudioManager.setGroupVolume( CarAudioManager.PRIMARY_AUDIO_ZONE, 0, volumeInAudioManagerUnit, AudioManager.FLAG_SHOW_UI | AudioManager.FLAG_PLAY_SOUND);
        }


    }


    public void setVolumeMute() {
        Log.d(TAG, "setVolumeMute ");
        mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_MUTE, 0);
    }


    /**
     * 获取系统最大媒体音量
     *
     * @return
     */
    public int getMaxMusicVolume() {
        return mAudioManager != null ? mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) : 15;
    }

    public VolumeChangeListener getVolumeChangeListener() {
        return mVolumeChangeListener;
    }

    public void setVolumeChangeListener(VolumeChangeListener volumeChangeListener) {
        this.mVolumeChangeListener = volumeChangeListener;
    }

    /**
     * 注册音量广播接收器
     *
     * @return
     */
    public void registerVolumeReceiver() {
        Log.d(TAG, "registerVolumeReceiver");
        mVolumeBroadcastReceiver = new VolumeBroadcastReceiver(this);
        IntentFilter filter = new IntentFilter();
        filter.addAction(VOLUME_CHANGED_ACTION);
        mContext.registerReceiver(mVolumeBroadcastReceiver, filter);
        mRegistered = true;
    }

    /**
     * 解注册音量广播监听器，需要与 registerReceiver 成对使用
     */
    public void unregisterVolumeReceiver() {
        if (mRegistered) {
            try {
                mContext.unregisterReceiver(mVolumeBroadcastReceiver);
                mVolumeChangeListener = null;
                mRegistered = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static class VolumeBroadcastReceiver extends BroadcastReceiver {
        private WeakReference<VolumeChangeManager> mObserverWeakReference;

        public VolumeBroadcastReceiver(VolumeChangeManager volumeChangeObserver) {
            mObserverWeakReference = new WeakReference<>(volumeChangeObserver);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            //媒体音量改变才通知
            if (VOLUME_CHANGED_ACTION.equals(intent.getAction())
                    && (intent.getIntExtra(EXTRA_VOLUME_STREAM_TYPE, -1) == AudioManager.STREAM_MUSIC)) {
                VolumeChangeManager observer = mObserverWeakReference.get();
                if (observer != null) {
                    VolumeChangeListener listener = observer.getVolumeChangeListener();
                    if (listener != null) {
                        int volume = observer.getCurrentMusicVolumeInAlexaUnit();
                        if (volume >= 0) {
                            listener.onVolumeChanged(volume);
                        }
                    }
                }
            }
        }
    }
}

