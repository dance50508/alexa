package com.mobiledrivetech.voiceassistant.alexa.bean;

public class NavigationEntity {

    private String name;
    private Slots slots;
    private String type;

    public NavigationEntity(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setSlots(Slots slots) {
         this.slots = slots;
     }
     public Slots getSlots() {
         return slots;
     }

    public void setType(String type) {
         this.type = type;
     }
     public String getType() {
         return type;
     }

}