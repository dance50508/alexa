package com.mobiledrivetech.voiceassistant.alexa.bean;

public class StockBean extends BaseBean{
    private String name;//股票名称
    private String code;//股票代号
    private String change;
    private String percentage;//
    /*private String date;//日期
    private String openPrice;//今日开盘股价
    private String lastClosePrice;//昨日收盘股价
    private String lowPrice;//最低股价
    private String highPrice;//最高股价*/
    private String currentPrice;//当前股价
    /*private String volume;//成交量
    private String amount;//成交额
    private String peTTM;//市盈率
    private String turnoverRate;//换手率
    private List<Double> prices;
    private List<String> times;*/

    /*private double maxValue;
    private double minValue;*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(String currentPrice) {
        this.currentPrice = currentPrice;
    }

    @Override
    public String toString() {
        return "StockBean{" +
                "name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", change='" + change + '\'' +
                ", percentage='" + percentage + '\'' +
                ", currentPrice='" + currentPrice + '\'' +
                '}';
    }
}
