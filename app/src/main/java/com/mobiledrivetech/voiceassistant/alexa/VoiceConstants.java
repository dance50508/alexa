package com.mobiledrivetech.voiceassistant.alexa;

public class VoiceConstants {
    public static final int IO_INPUT = 0;
    public static final int IO_OUTPUT = 1;
    public static final int STATUS_IDLE = 0;
    public static final int STATUS_LISTEN = 1;
    public static final int STATUS_RECOGNIZE = 2;
    public static final int STATUS_SPEAK = 3;
    public static final int DELAYED_TIME = 200;

    public static final boolean DEBUG = true;
    public static final String ONLINE_MUSIC_SERVICE_NAME = "com.mobiledrivetech.onemusic.service.MediaPlaybackService";

    public final static String ACTION_TALK = "com.mobiledrivetech.voiceassistant.action.TALK";
    public final static String ACTION_BROADCASTRECEIVER_CAMERA = "com.mobiledrivetech.camera.action.DMS_EN_ASSISTANT";
    public final static String ACTION_BROADCASTRECEIVER_TTS = "com.mobiledrivetech.action.VOICE_TTS";

    public static class WidgetType {
        public static final int TYPE_OUTPUT = 0;
        public static final int TYPE_INPUT = 1;
        public static final int TYPE_WIDGET_CONTENT = 2;
        public static final int TYPE_CLIENT_CONNECT = 3;
        public static final int TYPE_WIDGET_WEATHER = 4;
        public static final int TYPE_WIDGET_STOCK = 5;
        public static final int TYPE_WIDGET_HELP = 6;
        public static final int TYPE_WIDGET_IOT = 7;
        public static final int TYPE_WIDGET_NAVIGATION_LIST = 8;
        public static final int TYPE_ITEM_SINGLE_LINE_INDEX = 0;
        public static final int TYPE_ITEM_TWO_LINE_INDEX = 1;
        public static final int TYPE_ITEM_POI_LIST = 0;
        public static final int TYPE_ITEM_CONTACT_LIST = 1;
        public static final int TYPE_CBL_CODE = 9;
    }

    public static class Music {
        public static final String ONEMUSIC_PACKAGE = "com.mobiledrivetech.onemusic";
        public static final String BLUETOOTH_PACKAGE = "com.android.bluetooth";
        public static final String STATUS_PAUSE = "audio_pause";
        public static final String STATUS_RESUME = "audio_resume";
        public static final String STATUS_PRE = "audio_pre";
        public static final String STATUS_NEXT = "audio_next";
        public static final String ACTION_AUDIO = "action.car.audio.status";
        public static final String AUDIO_STATUS = "audio_status";
        public static final String AUDIO_PACKAGE = "audio_package";
        public static final String MediaPlayControlType_PREV = "PREV";
        public static final String MediaPlayControlType_NEXT = "NEXT";
        public static final String MediaPlayControlType_PAUSE = "PAUSE";
        public static final String MediaPlayControlType_STOP = "STOP";
        public static final String MediaPlayControlType_RESUME = "RESUME";
    }

    public static class MessageType {
        public static final int MESSAGE_INSERT = 0;
        public static final int MESSAGE_UPDATE = 1;
        public static final int MESSAGE_VOICE_STATUS = 2;
        public static final int MESSAGE_EXIT = 3;
        public static final int MESSAGE_NETWORK_UI = 4;
        public static final int MESSAGE_CHANGE_PAGE = 5;
    }

    public static final int SIM_UN_EXISTS = -1;
    public static final int SIM_ONE_EXISTS = 0;
    public static final int SIM_TWO_EXISTS = 1;


    public class CameraConfig {
        public static final String CAMERA_ASSISTANT_ACTION = "com.mobiledrivetech.camera.action.CAMERA_ASSISTANT";
        public static final String CAMERA_ID = "camera_id";
        public static final String CAMERA_ACTION = "camera_action";
        public static final String HOME_CAMERA_NAME = "home_camera_name";
        public static final String HOME_CAMERA_ADDRESS = "home_camera_address";
        public static final String ADDRESS_VALUE = "rtsp://fihtdc:fihtdc@192.168.3.205:554/stream1";

        public static final int CAMERA_ACTION_INVALID = -1;
        public static final int CAMERA_ACTION_CAPTURE = 0;
        public static final int CAMERA_ACTION_START_RECORD = 1;
        public static final int CAMERA_ACTION_STOP_RECORD = 2;

        /**
         * invalid camera id
         */
        public static final int INVALID_CAMERA_ID = -1;

        /**
         * ID: Front(Main) Camera
         */
        public static final int FRONT_CAMERA_ID = 0;
        /**
         * ID: Back(Sub) Camera
         */
        public static final int INSIDE_CAMERA_MAIN_ID = 1;
        public static final int INSIDE_CAMERA_SUB_ID = 2;
        public static final int AVM_CAMERA_ID = 3;
        public static final int AVM_CAMERA_FRONT_ID = 4;
        public static final int AVM_CAMERA_BACK_ID = 5;
        public static final int AVM_CAMERA_LEFT_ID = 6;
        public static final int AVM_CAMERA_RIGHT_ID = 7;
        public static final int USB_CAMERA_ID = 8;

    }

    public static class NavigationType {
        public static final String CUSTOM_NAV_APP_PACKAGE = "com.mobiledrivetech.navigationmap";
        public static final String CUSTOM_NAV_APP_MAIN_ACTIVITY = "com.mobiledrivetech.navigationmapui.MainMapActivity";
        public static final String CUSTOM_NAV_APP_ACTION = "com.mobiledrivetech.navigationmap.intent.action.start_navi";
        public static final String CUSTOM_NAV_SEARCH_ACTION = "com.mobiledrivetech.navigationmap.intent.action.show_search";
        public static final String CUSTOM_STOP_NAV_ACTION = "com.mobiledrivetech.navigationmap.intent.navi_exit";
        public static final String CUSTOM_NAV_APP_CATEGORY = "com.mobiledrivetech.navigationmap.intent.category.voice";
        public static final String CUSTOM_NAV_DATA_TITLE = "navi_title";
        public static final String CUSTOM_NAV_DATA_ADDRESS = "navi_address";
        public static final String CUSTOM_NAV_DATA_DISTANCE = "navi_distance";
        public static final String CUSTOM_NAV_DATA_LAT = "latitude";
        public static final String CUSTOM_NAV_DATA_LON = "longitude";

        public static final String CALL_METHOD_SEARCH_ROUTE_POI = "search_route_poi";
        public static final String SEARCH_ROUTE_POI_TYPE = "route_poi_type";
        public static final String SEARCH_KEY_PARKING = "parking";
        public static final String SEARCH_KEY_GAS = "gas";
        public static final String SEARCH_KEY_BATHROOM = "bathroom";
        public static final String SEARCH_KEY_RESTAURANT = "restaurant";
        public static final String SEARCH_KEY_SERVICE = "service";
        public static final String SEARCH_KEY_REPAIR = "repair";

        public static final String NAVIGATION_MAP_BIGGER = "navigation.map.bigger";
        public static final String NAVIGATION_MAP_SMALLER = "navigation.map.smaller";

    }

}
