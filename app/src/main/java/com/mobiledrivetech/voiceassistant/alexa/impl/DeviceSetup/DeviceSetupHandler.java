/*
 * Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *     http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.mobiledrivetech.voiceassistant.alexa.impl.DeviceSetup;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.Toast;

import com.amazon.aace.alexa.DeviceSetup;
import com.mobiledrivetech.voiceassistant.alexa.R;
import com.mobiledrivetech.voiceassistant.alexa.impl.Logger.LoggerHandler;

public class DeviceSetupHandler extends DeviceSetup {
    private static final String sTag = DeviceSetupHandler.class.getSimpleName();

    private final Activity mActivity;
    private final Context mContext;
    private final LoggerHandler mLogger;

    public DeviceSetupHandler(Context context, LoggerHandler logger) {
        mActivity = null;
        mContext = context;
        mLogger = logger;
    }

    public DeviceSetupHandler(Activity activity, LoggerHandler logger) {
        mActivity = activity;
        mContext = activity.getApplicationContext();
        mLogger = logger;
        setupGUI();
    }

    @Override
    public void setupCompletedResponse(final StatusCode statusCode) {
        mLogger.postInfo(sTag, "setupCompletedResponse " + statusCode.toString());
        if (mActivity == null)   return;
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mActivity, statusCode.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupGUI() {
        if (mActivity == null)  return;
        View logoutView = mActivity.findViewById(R.id.cblLogout);
        logoutView.findViewById(R.id.sendSetupCompleted).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLogger.postInfo(sTag, "Sending setupCompleted");
                setupCompleted();
            }
        });
    }
}
