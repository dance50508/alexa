package com.mobiledrivetech.voiceassistant.alexa.impl.Navigation;

import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.mobiledrivetech.voiceassistant.alexa.VoiceConstants;

public class NavigationController {
    private static final String TAG = NavigationController.class.getSimpleName();
    private static final String COLUMN_FAVORITE_NAME = "name";
    private static final String COLUMN_ADDRESS = "address";
    private static final String CUSTOM_NAV_DATA_LAT = "latitude";
    private static final String CUSTOM_NAV_DATA_LON = "longitude";
    private static final String CUSTOM_NAV_TYPE = "type";
    private static final int TYPE_HOME = 1;
    private static final int TYPE_COMPANY = 2;
    private static final Uri FAVORITE_URI = Uri.parse("content://com.mobiledrivetech.navigationmap/favorite");
    private static final Uri FREEURI = new Uri.Builder().scheme("content").authority("com.mobiledrivetech.navigationmap")
            .path("navigationcontrol").build();

    private static NavigationController mInstance;
    private Context mContext;
    private Bundle extras = new Bundle();

    private NavigationController(Context context) {
        this.mContext = context.getApplicationContext();
    }

    public static NavigationController getInstance(Context context) {
        if (mInstance == null) {
            synchronized (NavigationController.class) {
                if (mInstance == null) {
                    mInstance = new NavigationController(context);
                }
            }
        }
        return mInstance;
    }

    public void startCustomMapNavi(String title, String subTitle, double lng, double lat) {
        Log.i(TAG, " startCustomMapNavi  title : " + title + "  subTitle : " + subTitle + "  lng : " + lng + " lat : " + lat);
        try {
            Intent intent = new Intent(VoiceConstants.NavigationType.CUSTOM_NAV_APP_ACTION);
            intent.setClassName(VoiceConstants.NavigationType.CUSTOM_NAV_APP_PACKAGE, VoiceConstants.NavigationType.CUSTOM_NAV_APP_MAIN_ACTIVITY);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addCategory(VoiceConstants.NavigationType.CUSTOM_NAV_APP_CATEGORY);
            intent.putExtra(VoiceConstants.NavigationType.CUSTOM_NAV_DATA_TITLE, title);
            intent.putExtra(VoiceConstants.NavigationType.CUSTOM_NAV_DATA_ADDRESS, subTitle);
            intent.putExtra(VoiceConstants.NavigationType.CUSTOM_NAV_DATA_LON, lng);
            intent.putExtra(VoiceConstants.NavigationType.CUSTOM_NAV_DATA_LAT, lat);
            mContext.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopNavRoute() {
        Log.d(TAG, "stopNavRoute: ");
        Intent intent = new Intent();
        intent.setAction(VoiceConstants.NavigationType.CUSTOM_STOP_NAV_ACTION);
        mContext.sendBroadcast(intent);
    }

    public void doNavigationOperate(String type) {
        boolean isNaviTop = isNaviTop();
        boolean isNaviStarted = isNaviStarted();
        Log.i(TAG, "doNavigationOperate command : " + type + " isNaviStarted :" + isNaviStarted + " isNaviTop : " + isNaviTop);
        if (isNaviStarted && isNaviTop) {
            switch (type) {
                case VoiceConstants.NavigationType.NAVIGATION_MAP_BIGGER: {
                    if (doMapOperate("zoom_in")) {
                        zoomIn();
                        Log.d(TAG, "doNavigationOperate: 已放大");
                    } else {
                        Log.d(TAG, "doNavigationOperate: 已放大到最大");
                    }
                    break;
                }
                case VoiceConstants.NavigationType.NAVIGATION_MAP_SMALLER: {
                    if (doMapOperate("zoom_out")) {
                        zoomOut();
                        Log.d(TAG, "doNavigationOperate: 已缩小");
                    } else {
                        Log.d(TAG, "doNavigationOperate: 已縮小到最小");
                    }
                    break;
                }
            }
        } else if (!isNaviStarted && isNaviTop) {
            switch (type) {
                case VoiceConstants.NavigationType.NAVIGATION_MAP_BIGGER: {
                    if (doMapOperate("zoom_in")) {
                        zoomIn();
                        Log.d(TAG, "doNavigationOperate: 已放大");
                    } else {
                        Log.d(TAG, "doNavigationOperate: 已放大到最大");
                    }
                    break;
                }
                case VoiceConstants.NavigationType.NAVIGATION_MAP_SMALLER: {
                    if (doMapOperate("zoom_out")) {
                        zoomOut();
                        Log.d(TAG, "doNavigationOperate: 已缩小");
                    } else {
                        Log.d(TAG, "doNavigationOperate: 已縮小到最小");
                    }
                    break;
                }
            }
        } else if (!isNaviTop) {
        }
        if (isNaviStarted) {
            if (VoiceConstants.NavigationType.CUSTOM_STOP_NAV_ACTION.equals(type)) {
                Log.d(TAG, "doNavigationOperate: 已退出導航");
                stopNavRoute();
                toNavigationAppView(mContext);
            }
        }
    }

//    public void doNavigationOperate(String type) {
//        Log.i(TAG, "doNavigationOperate command : " + type + " isNaviStarted :" + isNaviStarted() + " isNaviTop : " + isNaviTop());
//        if (isNaviStarted() && isNaviTop()) {
//            switch (type) {
//                case VoiceConstants.FreeWakeupType.NAVIGATION_HOW_DRIVE:
//                    broadcastTip();
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_HOW_LONG:
//                    broadcastRemain();
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_LOOK_WHOLE:
//                    displayOverview();
//                    TXZTtsManager.getInstance().speakText("好的");
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_MAP_BIGGER:
//                    if (doMapOperate("zoom_in")) {
//                        TXZTtsManager.getInstance().speakText("已放大");
//                    } else {
//                        TXZTtsManager.getInstance().speakText("已放大到最大");
//                    }
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_MAP_SMALLER:
//                    if (doMapOperate("zoom_out")) {
//                        TXZTtsManager.getInstance().speakText("已缩小");
//                    } else {
//                        TXZTtsManager.getInstance().speakText("已缩小到最小");
//                    }
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_ROUTE_FREE:
//                    setRoutePreference(1);
//                    TXZTtsManager.getInstance().speakText("路线已调整");
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_ROUTE_HIGH:
//                    setRoutePreference(3);
//                    TXZTtsManager.getInstance().speakText("路线已调整");
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_ROUTE_JAM:
//                    setRoutePreference(0);
//                    TXZTtsManager.getInstance().speakText("路线已调整");
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_ROUTE_NOHIGH:
//                    setRoutePreference(2);
//                    TXZTtsManager.getInstance().speakText("路线已调整");
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_CAR_DIRECTION:
//                    setCarDirection(1);
//                    TXZTtsManager.getInstance().speakText("地图方向已调整");
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_NORTH_DIRECTION:
//                    setCarDirection(0);
//                    TXZTtsManager.getInstance().speakText("地图方向已调整");
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_SEARCH_PARKING:
//                    searchRoutePOI(VoiceConstants.NavigationType.SEARCH_KEY_PARKING);
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_SEARCH_GAS:
//                    searchRoutePOI(VoiceConstants.NavigationType.SEARCH_KEY_GAS);
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_SEARCH_BATHROOM:
//                    searchRoutePOI(VoiceConstants.NavigationType.SEARCH_KEY_BATHROOM);
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_SEARCH_RESTAURANT:
//                    searchRoutePOI(VoiceConstants.NavigationType.SEARCH_KEY_RESTAURANT);
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_SEARCH_SERVICE:
//                    searchRoutePOI(VoiceConstants.NavigationType.SEARCH_KEY_SERVICE);
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_SEARCH_REPAIR:
//                    searchRoutePOI(VoiceConstants.NavigationType.SEARCH_KEY_REPAIR);
//                    break;
//            }
//        } else if (!isNaviStarted() && isNaviTop()) {
//            switch (type) {
//                case VoiceConstants.FreeWakeupType.NAVIGATION_MAP_BIGGER:
//                    if (doMapOperate("zoom_in")) {
//                        TXZTtsManager.getInstance().speakText("已放大");
//                    } else {
//                        TXZTtsManager.getInstance().speakText("已放大到最大");
//                    }
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_MAP_SMALLER:
//                    if (doMapOperate("zoom_out")) {
//                        TXZTtsManager.getInstance().speakText("已缩小");
//                    } else {
//                        TXZTtsManager.getInstance().speakText("已缩小到最小");
//                    }
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_START:
//                    startNavi();
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_START_ONE:
//                    selectRoute(0);
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_START_TWO:
//                    selectRoute(1);
//                    break;
//                case VoiceConstants.FreeWakeupType.NAVIGATION_START_THREE:
//                    selectRoute(2);
//                    break;
//            }
//        } else if (!isNaviTop()) {
//            switch (type) {
//                case VoiceConstants.FreeWakeupType.NAVIGATION_BACK:
//                    Utils.toNavigationAppView(mContext);
//                    break;
//            }
//        }
//        if (isNaviStarted()) {
//            if (VoiceConstants.FreeWakeupType.NAVIGATION_STOP.equals(type)) {
//                TXZTtsManager.getInstance().speakText("已退出导航");
//                stopNavRoute();
//                Utils.toNavigationAppView(mContext);
//            }
//        }
//    }

    private boolean doMapOperate(String zoom) {
        return mContext.getContentResolver().call(FREEURI, zoom, null, extras).getBoolean("canZoom");
    }

    //地圖放大
    public void zoomIn() {
        mContext.getContentResolver().call(FREEURI, "zoom_in", null, extras);
    }

    public void zoomOut() {
        mContext.getContentResolver().call(FREEURI, "zoom_out", null, extras);
    }

    public void closeMap() {
        Log.d(TAG, "closeMap: ");
        mContext.getContentResolver().call(FREEURI, "move_map_to_back", null, extras);
    }

    private void setRoutePreference(int pre) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("navi_set_route_preference", pre); // 0 避免拥堵; 1 避免收费; 2 不走高速; 3 高速优先
        mContext.getContentResolver().update(FREEURI, contentValues, null, null);
    }

    private void setCarDirection(int set) {
        ContentValues contentValues1 = new ContentValues();
        contentValues1.put("navi_mode", set); // 0 跟随车头; 1 车头向上
        mContext.getContentResolver().update(FREEURI, contentValues1, null, null);
    }

    private void displayOverview() {
        mContext.getContentResolver().call(FREEURI, "display_overview", null, extras);
    }

    private void broadcastTip() {
        mContext.getContentResolver().call(FREEURI, "broadcast_tip", null, extras);
    }

    private void broadcastRemain() {
        mContext.getContentResolver().call(FREEURI, "broadcast_remain", null, extras);
    }

    public boolean isNaviStarted() {
        return mContext.getContentResolver().call(FREEURI, "is_navi_started", null, extras).getBoolean("isNaviStarted");
    }

    private void searchRoutePOI(String poiStr) {
        Bundle extras = new Bundle();
        extras.putString(VoiceConstants.NavigationType.SEARCH_ROUTE_POI_TYPE, poiStr);
        mContext.getContentResolver().call(FREEURI, VoiceConstants.NavigationType.CALL_METHOD_SEARCH_ROUTE_POI, null, extras);
    }

    public boolean isNaviTop() {
        Bundle extras = new Bundle();
        return mContext.getContentResolver().call(FREEURI, "is_in_front", null, extras).getBoolean("isNaviFront");
    }

    private void startNavi() {
        mContext.getContentResolver().call(FREEURI, "start_navi", null, extras);
    }

    private void selectRoute(int num) {
        Uri uri = new Uri.Builder().scheme("content").authority("com.mobiledrivetech.navigationmap").path("voice_assistant").build();
        Bundle data = new Bundle();
        data.putInt("select_route_id", num);
        mContext.getContentResolver().call(uri, "select_route", null, data);
    }

//    public FavoriteData getFavoriteAddress(String des) {
//        ContentResolver resolver = mContext.getContentResolver();
//        int type = "home".equals(des) ? TYPE_HOME : TYPE_COMPANY;
//        try {
//            Cursor cursor = resolver.query(FAVORITE_URI, null, "type=?", new String[]{String.valueOf(type)}, null);
//            if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
//                String name = cursor.getString(cursor.getColumnIndex(COLUMN_FAVORITE_NAME));
//                String address = cursor.getString(cursor.getColumnIndex(COLUMN_ADDRESS));
//                double latitude = cursor.getDouble(cursor.getColumnIndex(CUSTOM_NAV_DATA_LAT));
//                double longitude = cursor.getDouble(cursor.getColumnIndex(CUSTOM_NAV_DATA_LON));
//                cursor.close();
//                return new FavoriteData(name, address, latitude, longitude);
//            }
//            assert cursor != null;
//            cursor.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

//    public void setFavoriteAddress(String title, String subTitle, double lng, double lat, int type) {
//        ContentResolver resolver = mContext.getContentResolver();
//        try {
//            ContentValues contentValues = new ContentValues();
//            contentValues.put(CUSTOM_NAV_TYPE, type);
//            contentValues.put(COLUMN_FAVORITE_NAME, title);
//            contentValues.put(COLUMN_ADDRESS, subTitle);
//            contentValues.put(CUSTOM_NAV_DATA_LAT, lat);
//            contentValues.put(CUSTOM_NAV_DATA_LON, lng);
//            resolver.insert(FAVORITE_URI, contentValues);
//            Log.i(TAG, " setFavoriteAddress  title : " + title + "  subTitle : " + subTitle + "  lng : " + lng + " lat : " + lat);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public void setRoutePoiAddress(String title, String subTitle, double lng, double lat) {
        Intent intent = new Intent("com.mobiledrivetech.navigationmap.intent.add_way_point");
        intent.addCategory("com.mobiledrivetech.navigationmap.intent.category.voice");
        intent.putExtra("navi_title", title);
        intent.putExtra("latitude", lat);
        intent.putExtra("longitude", lng);
        intent.putExtra("navi_address", subTitle);
        mContext.sendBroadcast(intent);
        toNavigationAppView(mContext);
    }

    public static void toNavigationAppView(Context context) {
        Log.d(TAG, "toNavigationAppView: ");
        try {
            Intent intent = new Intent();
            intent.setComponent(new ComponentName(VoiceConstants.NavigationType.CUSTOM_NAV_APP_PACKAGE,
                    VoiceConstants.NavigationType.CUSTOM_NAV_APP_MAIN_ACTIVITY));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
